# Kihon Trainer

Application Android destinée aux pratiquants de Karaté (Shotokan).
Génère et dicte des séquences plus ou moins aléatoires de kihons (mouvements de base) à pratiquer en vue d'une préparation à l'épreuve des kihons demandée lors des passages de grades.

## Requirements

Android 7 ou supérieur.
L'application nécéssite la Synthèse Vocale Google pour pouvoir dicter l'épreuve à haute voix.
Les données pour les voix Françaises et Japonaises doivent être installées.

## Fonctionnalités & Options

- Génération d'une épreuve de Kihons avec ses différentes parties conformément au réglement de la FFK :
    - Kihon simple sur 3 pas
    - Enchaînement de kihons sur 3 pas
    - Enchaînement de kihons sur place, à gauche puis à droite
    - Enchaînement de kihons multidirectionnel, à gauche puis à droite
- Configuration du nombre de répétitions par partie
- Lecture de l'épreuve à voix haute via synthèse vocale, avec les termes/kihons en japonais, afin de reproduire l'ambiance d'un vrai examen
- Possibilité d'activer la Traduction en termes français
- 4 Niveaux de difficulté
- Listage des Kihons disponibles avec possibilité d'en exclure des épreuves générées
- Possibilité de générer les kihons avec ou sans déplacements/positions/niveaux

## À faire

- ~~Mouvements (pas chassé, croisé...)~~
- ~~Positions (kiba, kokutsu...)~~
- ~~Niveaux (Jodan, Shudan...)~~
- ~~Afficher la progression de l'épreuve~~
- ~~Mode "No spoil"~~
- ~~Mettre en pause/reprendre l'épreuve~~
- ~~Sauver les préférences~~
- ~~Choix voix~~
- Démonstrations vidéo
- ~~Messages d'erreur en cas de mauvais réglages/selections~~
- Ajout manuel de sequences de Kihons & possibilité de tirer depuis ceux-là en addition ou à la place de ceux générés aléatoirement

## Technique

Application codée en Dart et utilisant le framework Flutter.
Dépendances : voir pubspec.yaml