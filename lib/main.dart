/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/services.dart' show SystemChrome, SystemUiOverlayStyle;

import "const/strings.dart";
import 'classes/kihons.dart';
import 'classes/config.dart';
import 'classes/tts.dart';
import 'tabs/train.dart';
import 'tabs/options.dart';
import 'tabs/kihons.dart';

final Map<Tab, Widget> appTabs = {
  Tab(text: "Entraînement", icon: Icon(Icons.play_arrow)) : TrainingTab(),
  Tab(text: "Options", icon: Icon(Icons.settings)) : OptionsTab(),
  Tab(text: "Techniques", icon: Icon(const IconData(0xe800, fontFamily: "KihonTabIcon", fontPackage: null))) : KihonsTab(),
};

class Kihons extends StatefulWidget {
  @override
  _KihonsState createState() => _KihonsState();
}

class _KihonsState extends State<Kihons> {

  bool _ready = false;

  Future<void> _initApp() async {
    await tts.init();
    await globalConfig.load();
    await globalIndex.load();
  }

  @override
  void initState() {
    super.initState();
    _initApp().whenComplete(() => setState(() => _ready = true));
  }

  @override
  Widget build(BuildContext context) {
    // Firstrun popup
    if (_ready && globalConfig.firstrun) {
      Future.delayed(Duration(seconds: 0), () => showDialog(context: context, builder: (context) => FirstRunDialog()));
      globalConfig.firstran();
    }
    
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            elevation: 10,
              toolbarHeight: 106,
              leading: Padding(
                padding: const EdgeInsets.all(2),
                child: Image.asset(UIString.AppIcon),
              ),
              title: Text(UIString.AppName),
              actions: <Widget>[IconButton(
                icon: Icon(Icons.help),
                onPressed: () async => await showDialog(context: context, builder: (context) => KTAboutDIalog()),
              )
              ],
              bottom: TabBar(
                tabs: appTabs.keys.toList(),
                indicatorWeight: 2,
              )
          ),
          body: _ready ? TabBarView(children: appTabs.values.toList())
              : Container(
              constraints: BoxConstraints.expand(),
              decoration: BoxDecoration(
                  image: DecorationImage(
                    scale: 2,
                    image: AssetImage(UIString.MainBG),
                  )
              ),
              child: Center(
                  child: Wrap(
                    spacing: 10,
                    direction: Axis.vertical,
                    crossAxisAlignment: WrapCrossAlignment.center,
                    children: [
                      CircularProgressIndicator(),
                      Text(UIString.Loading),
                    ],
              ))),
        ),
    );
  }
}

void main() {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Colors.transparent));
  runApp(MaterialApp(
    home: Kihons(),
    theme: ThemeData(
      primarySwatch: Colors.red,
      primaryColor: Colors.red[900],
      accentColor: Colors.white,
      toggleableActiveColor: Colors.red,
      brightness: Brightness.dark,
    ),
  ));
}

class KTAboutDIalog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return(
        AlertDialog(
          scrollable: true,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Image.asset(UIString.AppIcon, height: 48,),
              Column(
                  children: [
                    Text(UIString.AppName),
                    Text("ver. ${UIString.AppVer}", textScaleFactor: 0.7,),
                    Text(UIString.AppLic, textScaleFactor: 0.5,)
                  ]),
            ],
          ),
          content: Text("${UIString.AppDesc}"),
          actions: <Widget>[
            TextButton(
              child: Text(UIString.Gitlab),
              onPressed: () async => await launch(UIString.AppSrc),
            ),
            TextButton(
              child: Text(UIString.Contact),
              onPressed: () async => await launch("mailto:${UIString.AppMail}"),
            ),
            TextButton(
                child: Text(UIString.Ok),
                onPressed: () => Navigator.of(context).pop()
            ),
          ],
        )
    );
  }
}

class FirstRunDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      scrollable: true,
      title: Text(UIString.FirstRunTitle),
      content: Text(UIString.FirstRunContent),
      actions: <Widget>[
        TextButton(
          child: Text(UIString.Contact),
          onPressed: () async => await launch("mailto:${UIString.AppMail}"),
        ),
        TextButton(
            child: Text(UIString.Ok),
            onPressed: () => Navigator.of(context).pop()
        ),
      ],
    );
  }
}
