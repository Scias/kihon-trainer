/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'package:kihon_trainer/const/kihons.dart';

import '../classes/kihons.dart';
import '../const/common.dart';
import '../ui-common.dart';
import '../const/strings.dart';

class BackHeader extends StatelessWidget {
  final String text;
  BackHeader(this.text);
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(),
        )
      ),
      child: Material(
        elevation: 5,
        color: Theme.of(context).dialogBackgroundColor,
        child: ListTile(
          title: Text(this.text, style: TextStyle(fontWeight: FontWeight.bold, shadows: [Shadow(blurRadius: 5)])),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          ),
        ),
      ),
    );
  }
}

class KihonListView extends StatefulWidget {
  final KihonType type;
  KihonListView(this.type);

  @override
  _KihonListViewState createState() => _KihonListViewState();
}

class _KihonListViewState extends State<KihonListView> {

  List<Kihon> kihons;

  String get _bgImg {
    switch(this.widget.type) {
      case KihonType.def:
        return UIString.DefBG;
      case KihonType.hand:
        return UIString.HandBG;
      case KihonType.kick:
        return UIString.KickBG;
    }
  }

  @override
  void initState() {
    super.initState();
    kihons = DefaultKihons.values.where((k) => k.type == this.widget.type).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Scrollbar(
        thickness: 2,
        child: Container(
          constraints: BoxConstraints.expand(),
          decoration: BoxDecoration(
              image: DecorationImage(
                scale: 2,
                image: AssetImage(_bgImg),
              )
          ),
          child: ListView.builder(
              itemCount: kihons.length,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                    color: Colors.transparent,
                    border: index < kihons.length-1 ? Border(bottom: BorderSide(color: Theme.of(context).dividerColor)) : null,
                  ),
                  child: Material(
                    color: index.isEven ? Colors.black12 : Colors.black26,
                    child: CheckboxListTile(
                        dense: true,
                        isThreeLine: true,
                        secondary: IconButton(
                          icon: Icon(Icons.play_circle_outline),
                          onPressed: kihons[index].video ? () async => await showDialog(context: context, builder: (context) => KihonPopup(kihons[index])) : null,
                        ),
                        title: Text(kihons[index].format(), style: TextStyle(fontWeight: FontWeight.bold)),
                        subtitle: Text(kihons[index].trad),
                        value: globalIndex.kihonList[kihons[index].id],
                        onChanged: (bool actif) async {
                          await globalIndex.toggle(kihons[index].id, actif);
                          setState(() {});
                        }
                    ),
                  ),
                );
              }
          ),
        ),
      ),
    );
  }
}

class SeqsView extends StatelessWidget {

  List<Widget> _seqsList(Color sepColor, Color bgColor) {
    List<Widget> list = [];
    for (final EpreuvePart p in EpreuvePart.values) {
      final Iterable<KihonSeq> partSeqs = globalIndex.allSeqs.where((seq) => seq.partid == p);
      bool altBg = true;
      final List<Widget> seqList = partSeqs.map((seq) {
        altBg = !altBg;
        return Container(
          width: double.maxFinite,
            padding: EdgeInsets.all(8),
            decoration: BoxDecoration(
                color: altBg ? Colors.black26 : Colors.black38,
                border: Border(
                bottom: BorderSide(color: sepColor)
              )
            ),
            child: SeqDisplay(seq, tappable: true)
        );
      }).toList();
      list.add(
          ExpansionTile(
            backgroundColor: bgColor,
            initiallyExpanded: false,
            title: Text(EpreuveParts[p].name, style: TextStyle(fontWeight: FontWeight.bold, shadows: [Shadow(blurRadius: 5)])),
            subtitle: Text(EpreuveParts[p].desc),
            children: seqList,
          )
      );
    }
    return list;
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Scrollbar(
          thickness: 2,
          child: ListView(
            children: _seqsList(Theme.of(context).dividerColor, Theme.of(context).dialogBackgroundColor),
          )
        )
    );
  }
}

class SubPage extends StatelessWidget {
  SubPage(this.route);
  final String route;
  final Map<String, KihonType> routeType = {
    'P' : KihonType.hand,
    'K' : KihonType.kick,
    "D" : KihonType.def,
  };

  String _subTitle() {
    switch (route) {
      case 'P': case 'K': case 'D': case 'M': case 'S': return KihonTypes[routeType[this.route]].name;
      case 'KS': return UIString.PreSeqs;
      default: return "?";
    }
  }

  Widget _subWidget() {
    switch (route) {
      case 'P': case 'K': case 'D': return KihonListView(routeType[route]);
//      case 'M': case 'S': return ModView(this.route);
      case 'KS': return SeqsView();
      default: return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).scaffoldBackgroundColor,
      child: Column(
        children: [
          BackHeader(_subTitle()),
          _subWidget(),
        ],
      ),
    );
  }
}

// class ModView extends StatelessWidget {
//   final String type;
//   ModView(this.type);
//
//   List<Widget> _modList() {
//     List<Widget> list = [];
//     Map source;
//     String suffix, prefix;
//     if (this.type == 'S') {
//       source = Stances;
//       prefix = "Position";
//       suffix = "Dachi";
//     } else {
//       source = Moves;
//       prefix = "Un";
//       suffix = "Ashi";
//     }
//     for (final mod in source.values) {
//       list.add(
//           Card(
//               margin: const EdgeInsets.symmetric(vertical: 1.0),
//               child: ListTile(
//                 //leading: ColoredModBox("TET", Colors.red),
//                 dense: true,
//                 title: Text("${mod["nom"]} $suffix"),
//                 subtitle: Text("$prefix ${mod["trad"]}"),
//               )
//           )
//       );
//     }
//     return list;
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return Expanded(
//       child: Scrollbar(
//         thickness: 2,
//         child: ListView(
//           children: _modList(),
//         ),
//       ),
//     );
//   }
// }


class SubPageButton extends StatelessWidget {
  SubPageButton(this.icon, this.text, this.route, [this.sub]);
  final String route;
  final String icon;
  final String text;
  final String sub;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      child: ListTile(
        leading: this.icon.isNotEmpty ? Image.asset(this.icon) : SizedBox(),
        title: Text(this.text, style: TextStyle(fontWeight: FontWeight.bold, shadows: [Shadow(blurRadius: 5)])),
        subtitle: this.sub != null ? Text(this.sub, style: Theme.of(context).textTheme.caption) : SizedBox(),
        trailing: Icon(Icons.arrow_forward),
        onTap: () => Navigator.pushNamed(context, this.route),
      )
    );
  }
}

class KihonsHome extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Scrollbar(
      thickness: 2,
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 1),
          child: Column(
            children: [
              // Kihon
              SizedBox(height: 8),
              Text(UIString.KihonTabDesc, textAlign: TextAlign.center,),
              SizedBox(height: 8),
              SubPageButton(UIString.DefIcon, KihonTypes[KihonType.def].name, "D", "Uke"),
              SubPageButton(UIString.HandIcon, KihonTypes[KihonType.hand].name, "P", "Tsuki / Uchi"),
              SubPageButton(UIString.KickIcon, KihonTypes[KihonType.kick].name, "K", "Geri"),
              // Séquences
              SizedBox(height: 8),
              Text(UIString.SeqsDesc, textAlign: TextAlign.center),
              SizedBox(height: 8),
              SubPageButton(UIString.SeqsIcon, UIString.PreSeqs, "KS", "Séquences préenregistrées"),
              // Réinitialisation
              SizedBox(height: 8),
              Text(UIString.ReinitDesc, textAlign: TextAlign.center),
              SizedBox(height: 8),
              ElevatedButton.icon(
                icon: Icon(Icons.refresh),
                onPressed: () async => await showDialog(context: context, builder: (context) => ResetDialog()),
                label: Text(UIString.ReinitButton),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class ResetDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(UIString.ResetDialogTitle),
      content: Text(UIString.ResetDialogText),
      actions: [
        TextButton(
          child: Text(UIString.Ok),
          onPressed: () async {
            await globalIndex.reset();
            Navigator.pop(context);
          },
        ),
        TextButton(
          child: Text(UIString.Cancel),
          onPressed: () => Navigator.pop(context),
        )
      ],
    );
  }
}


class KihonsTab extends StatefulWidget {
  @override
  _KihonsTabState createState() => _KihonsTabState();
}

class _KihonsTabState extends State<KihonsTab> with AutomaticKeepAliveClientMixin {
  final GlobalKey<NavigatorState> _key = GlobalKey<NavigatorState>();

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    return WillPopScope(
      onWillPop: () async => !await _key.currentState.maybePop(),
      child: Navigator(
        key: _key,
        initialRoute: 'home',
        onGenerateRoute: (RouteSettings s) => MaterialPageRoute(builder: (context) => s.name == 'home' ? KihonsHome() : SubPage(s.name)),
      ),
    );
  }
}