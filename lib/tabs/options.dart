/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'package:flutter_material_pickers/flutter_material_pickers.dart';

import '../classes/config.dart';
import '../const/options.dart';
import '../const/strings.dart';

class SingleOption extends StatefulWidget {
  final Option option;
  SingleOption(this.option);

  @override
  _SingleOptionState createState() => _SingleOptionState();
}

class _SingleOptionState extends State<SingleOption> {

  Widget get _optionTitle {
    if (widget.option.tooltip == null) {
      return Text(widget.option.desc);
    }
    else return Wrap(
          crossAxisAlignment: WrapCrossAlignment.center,
          spacing: 5,
          children: [
            Text(widget.option.desc),
            GestureDetector(
                child: Icon(Icons.help, size: 20),
                onTap: () async => await showDialog(
                    context: context,
                    builder: (context) => Dialog(
                        child: Container(
                            padding: EdgeInsets.all(10),
                            child: Text(widget.option.tooltip)
                        )
                    )
                )
            )
          ]
    );
  }

  // Rebuild voix si changement via initTTS
  @override
  void initState() {
    super.initState();
    if (widget.option.type == OptionType.voice) {
      widget.option.voices.addListener(() => setState((){}));
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.option.type == OptionType.voice) {
      widget.option.voices.removeListener(() => setState((){}));
    }
  }
  ////

  @override
  Widget build(BuildContext context) {

    Widget optionWidget;

    final Function saveOption = (dynamic newValue) async {
      widget.option.setTo(newValue);
      await globalConfig.saveConfig();
      setState(() {});
    };

    switch(widget.option.type) {
      case OptionType.check:
        optionWidget = SwitchListTile(
          title: _optionTitle,
          subtitle: widget.option.sub != null
              ? Text(widget.option.sub)
              : null,
          isThreeLine: widget.option.sub != null ? true : false,
          value: widget.option.actif,
          onChanged: saveOption,
        );
        break;
      case OptionType.input:
        optionWidget = ListTile(
            title: _optionTitle,
            subtitle: widget.option.sub != null
                ? Text(widget.option.sub)
                : null,
            isThreeLine: widget.option.sub != null ? true : false,
            trailing: Container(
              padding: EdgeInsets.fromLTRB(0, 0, 0, 5),
              width: 50.0,
              //height: 50.0,
              child: TextButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all(Colors.black38),
                ),
                child: Text(widget.option.value.toString(), style: Theme.of(context).textTheme.button),
                onPressed: () =>
                    showMaterialNumberPicker(
                      context: context,
                      title: UIString.NumberSelect,
                      maxNumber: 20,
                      minNumber: 0,
                      confirmText: UIString.Confirm,
                      cancelText: UIString.Cancel,
                      selectedNumber: widget.option.value,
                      onChanged: saveOption,
                    ),
              ),
            )
        );
        break;
      case OptionType.select:
      case OptionType.voice:
        int i = 0;
        List<DropdownMenuItem> choix = [];
        for (String c in widget.option.getChoices()) {
          Widget w;
          if (widget.option.type == OptionType.voice)
            {
              // TODO Optimiser
              final String flag = c.contains("FR") ? UIString.FlagFR : UIString.FlagJP;
              w = Wrap(spacing: 5, crossAxisAlignment: WrapCrossAlignment.center,children: [Image.asset(flag, height: 16,),Text(c.substring(4))]);
            }
          else w = Text(c);
          choix.add(DropdownMenuItem(value: i, child: w));
          i++;
        }
        optionWidget = ListTile(
            title: _optionTitle,
            subtitle: widget.option.sub != null
                ? Text(widget.option.sub)
                : null,
            isThreeLine: widget.option.sub != null ? true : false,
            trailing: choix.isNotEmpty ? DropdownButton(
              value: widget.option.rawValue,
              icon: Icon(Icons.arrow_downward),
              items: choix,
              onChanged: saveOption,
            ) : Wrap(
              direction: Axis.vertical,
              crossAxisAlignment: WrapCrossAlignment.center,
              children: [
                Icon(Icons.error, color: Colors.orange),
                Text("Aucune voix détectée", style: Theme.of(context).textTheme.caption),
              ],
            )
        );
        break;
      default: // SPACER
        return Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(widget.option.sub, textAlign: TextAlign.center),
        );
    }

    return Card(
      margin: EdgeInsets.symmetric(vertical: 1.0),
      child: optionWidget,
    );

  }

}


class OptionsTab extends StatefulWidget {
  @override
  _OptionsTabState createState() => _OptionsTabState();
}

class _OptionsTabState extends State<OptionsTab> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final List<Widget> optionWidgets = globalConfig.options.values.map<Widget>((Option o) => SingleOption(o)).toList();

    return Scrollbar(
      thickness: 2.0,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 1),
        child: ListView(
          children: optionWidgets,
        ),
      ),
    );
  }
}