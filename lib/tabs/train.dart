/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:android_intent/android_intent.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:animations/animations.dart';

import 'train-provider.dart';
import '../classes/kihons.dart';
import '../classes/config.dart';
import '../const/common.dart';
import '../const/strings.dart';
import '../ui-common.dart';

final trainStateProvider = ChangeNotifierProvider((_) => TrainState());

class TrainingTab extends StatefulWidget {
  @override
  _TrainingTabState createState() => _TrainingTabState();
}

class _TrainingTabState extends State<TrainingTab> with AutomaticKeepAliveClientMixin {

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return ProviderScope(
      child: Column(
        children: [
          Expanded(child: EpreuveBody()),
          Footer(),
        ],
      ),
    );
  }
}

class EpreuveBody extends ConsumerWidget {

  // AFFICHAGE/FORMATAGE EPREUVE

  Widget _mkPartBox(EpreuvePart partid, List<Widget> seqs) {
    seqs.insert(0, Container(
      margin: EdgeInsets.fromLTRB(10, 5, 2, 5),
      child: Text(EpreuveParts[partid].name.toUpperCase(),
        style: TextStyle(
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
      ),
    ));
    return Container(
      width: double.infinity,
      child: Card(
        shape: ContinuousRectangleBorder(),
        child: Column(children: seqs),
        color: Colors.red[900],
        margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
      ),
    );
  }

  Widget _showEpreuve(Epreuve e, EpreuvePart partid, int seqid, Color bcolor, bool dirty, Key epreuveKey) {
    List<Widget> metaBoxes = [];
    List<Widget> seqBoxes;
    metaBoxes.add(SizedBox(height: 5));
    e.seqs.forEach((part, seqs) {
      seqBoxes = [];
      for (int i = 0; i < seqs.length; i++) {
        final bool active = part == partid && i == seqid ? true : false;
        bool nospoil;
        if (dirty || !globalConfig.get("nospoil").actif) nospoil = false;
        else {
          if (partid == null || seqid == null) nospoil = true;
          else {
            if (partid.index > part.index) nospoil = false;
            else if (partid.index == part.index && seqid >= i) nospoil = false;
            else nospoil = true;
          }
        }
        seqBoxes.add(Container(
            margin: EdgeInsets.symmetric(horizontal: 2, vertical: 1),
            padding: EdgeInsets.symmetric(horizontal: 5, vertical: 4),
            color: active ? Colors.white : bcolor,
            child: Row(
              children: [
                Text((i+1).toString()+". ", textAlign: TextAlign.left, style: TextStyle(color: active ? Colors.black : null)),
                Expanded(
                    child: nospoil ?
                    Center(child: Text("- ? -", style: TextStyle(color: Colors.grey))) :
                    SeqDisplay(seqs[i], active: active, tappable: true)
                ),
              ],
            )
        ));
      }
      seqBoxes.add(SizedBox(height: 1));
      metaBoxes.add(_mkPartBox(part, seqBoxes));
    });

    return Scrollbar(
      key: epreuveKey,
      thickness: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 1),
        child: SingleChildScrollView(
          child: Column(
              children: metaBoxes
          ),
        ),
      ),
    );
  }

  // WIDGET INFO SI PAS EPREUVE / ERREUR

  Widget _infoWidget(bool err) {
    Icon icon;
    String title, subtitle;
    if (err) {
      icon = Icon(Icons.error, color: Colors.red, size: 40);
      title = UIString.InfoErrTitle;
      subtitle = UIString.InfoErrSub;
    }
    else {
      icon = Icon(Icons.info, color: Colors.blueAccent, size: 40);
      title = UIString.InfoTitle;
      subtitle = UIString.InfoSub;
    }

    return Column(
      key: UniqueKey(),
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(20, 50, 20, 20),
          child: Column(
            children: [
              icon,
              SizedBox(height: 10),
              Text(title, style: TextStyle(fontSize: 24)),
              SizedBox(height: 10),
              Text(subtitle, textAlign: TextAlign.center),
            ]
          ),
        ),
        EpreuveButton()
      ],
    );
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final train = watch(trainStateProvider);

    return Container(
      constraints: BoxConstraints.expand(),
      decoration: BoxDecoration(
          image: DecorationImage(
            //fit: BoxFit.scaleDown,
            scale: 2,
            image: AssetImage(UIString.MainBG),
          )
      ),
      child: PageTransitionSwitcher(
        transitionBuilder: (child, anim1, anim2) => FadeThroughTransition(
          fillColor: Colors.transparent,
            animation: anim1,
            secondaryAnimation: anim2,
            child: child
        ),
        duration: Duration(milliseconds: 400),
        child: train.epreuve == null ? _infoWidget(train.erreur) : _showEpreuve(train.epreuve, train.partid, train.seqid, Theme.of(context).cardColor, train.dirty, train.epreuveKey)
      ),
    );

  }
}

class Footer extends ConsumerWidget {

  // POPUP LÉGENDE

  List<Widget> _legendeBuilder() {
    List<Widget> legende = [];
    bool first = true;
    Map attrList;

    for (final AttrType attr in AttrType.values) {
      final String catName = AttrTypes[attr].name;
      final Color catColor = AttrTypes[attr].color;
      final String catSuffix = AttrTypes[attr].suffix != null ? AttrTypes[attr].suffix : "";

      if (!first) legende.add(Divider());

      first = false;

      legende.add(Text(catName, style: TextStyle(fontWeight: FontWeight.bold)));
      legende.add(SizedBox(height: 5));

      switch(attr) {
        case AttrType.movement: attrList = Movements; break;
        case AttrType.position: attrList = Positions; break;
        case AttrType.mod: attrList = Modifiers; break;
        case AttrType.level: attrList = Levels; break;
      }

      for (final CommonStruct a in attrList.values) {
        final String tag = a.tag;
        final String attName = a.name;

          legende.add(Row(
            children: [
              SizedBox(width: 32, child: ColoredModBox(tag, catColor)),
              SizedBox(width: 10),
              Text("$attName $catSuffix"),
            ]
          ));
      }
    }
    return legende;
  }

  AlertDialog _legendDialog(BuildContext ctx) {
    return AlertDialog(
      scrollable: true,
      content: Column(
        children: _legendeBuilder(),
      ),
    );
  }

  //

  Widget _statusBand(RunState state, bool busy) {
    Widget statusIcon;
    Widget statusText;

    if (!busy) {
      if (state == RunState.running) {
        statusIcon = Center(child: Icon(Icons.volume_up_outlined, size: 34, color: Colors.lightGreenAccent));
        statusText = Text(UIString.Running, style: TextStyle(fontWeight: FontWeight.bold));
      }
      else if (state == RunState.paused) {
        statusIcon = Icon(Icons.pause_circle_outline, size: 36, color: Colors.orange);
        statusText = Text(UIString.Paused, style: TextStyle(fontWeight: FontWeight.bold));
      }
    } else {
      statusIcon = Padding(
        padding: const EdgeInsets.all(8),
        child: CircularProgressIndicator(strokeWidth: 4),
      );
      if (state == RunState.starting) statusText = Text(UIString.Starting);
      else if (state == RunState.pausing) statusText = Text(UIString.Pausing);
      else if (state == RunState.stopping) statusText = Text(UIString.Stopping);
      else statusText = SizedBox();
    }

    return Container(
      key: UniqueKey(),
      height: 36,
      margin: EdgeInsets.fromLTRB(0,0,0,4),
      child: Row(
        children: [
          SizedBox(width: 30),
          SizedBox(width: 36, child: statusIcon),
          SizedBox(width: 10),
          Expanded(child: statusText),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final train = watch(trainStateProvider);

    return AnimatedCrossFade(
      sizeCurve: Curves.easeInOut,
      duration: Duration(milliseconds: 500),
      crossFadeState: train.epreuve == null ? CrossFadeState.showFirst : CrossFadeState.showSecond,
      firstChild: Container(width: double.infinity),
      secondChild: Column(
        children: [
          train.state == RunState.running ?
          LinearProgressIndicator(minHeight: 2, valueColor: AlwaysStoppedAnimation<Color>(Colors.lightGreenAccent))
              : Container(height: 2, color: train.state == RunState.paused ? Colors.orange : Colors.grey),
          Container(
            color: Theme.of(context).scaffoldBackgroundColor,
            padding: EdgeInsets.symmetric(horizontal: 0, vertical: 10),
            child: Column(
              children: [
                PageTransitionSwitcher(
                  transitionBuilder: (child, anim1, anim2) =>
                      SharedAxisTransition(
                        animation: anim1,
                        secondaryAnimation: anim2,
                        child: child,
                        transitionType: SharedAxisTransitionType.horizontal,
                      ),
                  duration: Duration(milliseconds: 250),
                  child: train.state != RunState.stopped ? _statusBand(train.state, train.busy) : EpreuveButton(),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    TextButton.icon(
                        onPressed: train.state != RunState.stopped ? train.busy ? null : train.stop : null,
                        label: Text(UIString.Stop),
                        icon: Icon(Icons.stop_circle_outlined)
                    ),
                    TtsButton(),
                    TextButton.icon(
                        onPressed: () async => await showDialog(context: context, builder: (context) => _legendDialog(context)),
                        label: Text(UIString.Legend),
                        icon: Icon(Icons.live_help_rounded),
                        style: TextButton.styleFrom(primary: Colors.white)
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class TtsButton extends ConsumerWidget {

  final AlertDialog _firstTTSDialog = AlertDialog(
    scrollable: true,
    title: Text(UIString.FirstTTSTitle),
    content: Text(UIString.FirstTTSContent),
  );

  // DIALOG À AFFICHER SI TTS NON FONCTIONNEL
  final AlertDialog _noTTSDialog = AlertDialog(
    scrollable: true,
    title:  Text(UIString.NoTTSTitle),
    content: Text(UIString.NoTTSContent),
    actions: [
      TextButton(
        child: Text(UIString.GTTSPlayStore),
        onPressed: () async => AndroidIntent(
          action: UIString.GTTSPlayStoreAction,
          data: Uri.encodeFull(UIString.GTTSPlayStoreLink),
        ).launch(),
      ),
      TextButton(
        child: Text(UIString.TTSSettings),
        onPressed: () async => AndroidIntent(
          action: UIString.IntentAction,
          package: UIString.SettingsPackage,
          componentName: UIString.TTSIntentSettings,
        ).launch(),
      ),
      TextButton(
        child: Text(UIString.LocalVoices),
        onPressed: () async => AndroidIntent(
          action: UIString.IntentAction,
          package: UIString.TTSPackage,
          componentName: UIString.TTSIntentLocalVoices,
        ).launch(),
      )
    ]
  );

  @override
  Widget build(BuildContext context, ScopedReader watch) {
    final train = watch(trainStateProvider);
    Icon buttonIcon;
    Color buttonColor;
    String buttonLabel;
    Function func;
    Key ukey;

    switch (train.state) {
      case RunState.stopped:
      case RunState.stopping:
        ukey = ValueKey(1);
        buttonLabel = UIString.Start;
        buttonIcon = Icon(Icons.volume_up_outlined);
        buttonColor = Colors.green;
        func = train.busy ? null : () async {
          // TODO Popup 1st TTS
          if (await train.testTts()) train.start();
          else await showDialog(context: context, builder: (context) => _noTTSDialog);
        };
        break;
      case RunState.running:
      case RunState.starting:
        ukey = ValueKey(2);
        buttonLabel = UIString.Pause;
        buttonIcon = Icon(Icons.pause_circle_outline_outlined);
        buttonColor = Colors.deepOrangeAccent;
        func = train.busy ? null : train.pause;
        break;
      case RunState.paused:
      case RunState.pausing:
        ukey = ValueKey(3);
        buttonLabel = UIString.Resume;
        buttonIcon = Icon(Icons.not_started_outlined);
        buttonColor = Colors.blueGrey;
        func = train.busy ? null : train.resume;
        break;
      default: // Évite potentiel crash
        ukey = ValueKey(4);
        buttonLabel = "?";
        buttonIcon = Icon(Icons.texture);
        break;
    }

    return Container(
      key: ukey,
      width: 140,
      child: ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.resolveWith((states) => states.contains(MaterialState.disabled) ? Theme.of(context).disabledColor : buttonColor),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)
          )),
        ),
        onPressed: func,
        icon: buttonIcon,
        label: Text(buttonLabel),
      ),
    );

  }
}

class EpreuveButton extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    final bool exists = context.read(trainStateProvider).epreuve != null;
    return Container(
      height: 36,
      margin: EdgeInsets.fromLTRB(0,0,0,4),
      child: ElevatedButton.icon(
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(Colors.indigo),
          shape: MaterialStateProperty.all(RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10)
          )),
        ),
        onPressed: context.read(trainStateProvider).genEpreuve,
        label: exists ? Text(UIString.GenNew) : Text(UIString.Gen),
        icon: exists ? Icon(Icons.replay_circle_filled) : Icon(Icons.assignment),
      ),
    );
  }
}