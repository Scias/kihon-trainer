/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';

import '../classes/config.dart';
import '../classes/kihons.dart';
import '../classes/tts.dart';
import '../const/common.dart' show RunState, EpreuvePart;

class TrainState extends ChangeNotifier {
  Epreuve epreuve;
  Key epreuveKey;
  Widget epreuveWidget;
  RunState state = RunState.stopped;
  bool erreur = false;
  bool busy = false;
  bool dirty = false;
  EpreuvePart partid;
  int seqid;

  void _setProgress([EpreuvePart partid, int seqid]) {
    this.partid = partid;
    this.seqid = seqid;
  }

  void _setBusy(bool state) {
    this.busy = state;
    notifyListeners();
  }

  KihonGenRules _getRules() {
    Map<EpreuvePart, int> seqsPerPart = {};
    for (final EpreuvePart p in EpreuvePart.values) {
      final int seqNb = globalConfig.get(describeEnum(p)).value;
      if (seqNb > 0) seqsPerPart[p] = seqNb;
    }
    if (seqsPerPart.isEmpty) return null;
    else {
      return KihonGenRules(
        seqsPerPart: seqsPerPart,
        mode: globalConfig.get("mode").value,
        diff: globalConfig.get("diff").value,
        withMvt: globalConfig.get("mvt").value,
        withPos: globalConfig.get("pos").value,
        withMod: globalConfig.get("mod").value,
        withNiv: globalConfig.get("niv").value,
      );
    }
  }

  void genEpreuve() {
    this._setProgress();
    this.dirty = globalConfig.get("nospoil").actif ? false : true;
    final KihonGenRules rules = _getRules();
    if (rules != null) this.epreuve = Epreuve(rules);
    if (rules == null || this.epreuve.isEmpty) {
      this.epreuve = null;
      this.erreur = true;
    }
    epreuveKey = UniqueKey();
    notifyListeners();
  }

  void start() async {
    if (this.state == RunState.stopped) {
      this.state = RunState.running;
      notifyListeners();

      Wakelock.enable();
      await for (Progress s in tts.dictEpreuve(this.epreuve)) {
        if (tts.state != RunState.stopped) {
          this._setProgress(s.partid, s.seqid);
          notifyListeners();
        } else break;
      }
      Wakelock.disable();

      this.state = RunState.stopped;
      this._setProgress();
      this.dirty = true;
      this._setBusy(false);
    }
  }

  void pause() async {
    Wakelock.disable();
    this.state = RunState.pausing;
    this._setBusy(true);
    await tts.pauseEpreuve();
    this.state = RunState.paused;
    this._setBusy(false);
  }

  void resume() {
    Wakelock.enable();
    this.state = RunState.running;
    tts.resumeEpreuve();
    notifyListeners();
  }

  void stop() async {
    this.state = RunState.stopping;
    this._setBusy(true);
    await tts.stopEpreuve();
  }

  // Tester le TTS avant de lancer l'épreuve
  Future<bool> testTts() async {
    this.state = RunState.starting;
    this._setBusy(true);

    if (!tts.ready) await tts.initTTS();

    final bool res = tts.ready ? await tts.test() : false;

    this.state = RunState.stopped;
    this._setBusy(false);

    return res;
  }

}