/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:dio/dio.dart';

import 'classes/kihons.dart';
import 'const/strings.dart';
import 'const/kihons.dart';
import 'const/common.dart';

class VideoAsset {
  final String slug;
  File file;

  bool get exists => this.file.existsSync();

  VideoAsset(this.slug) {
    this.file = File("${globalIndex.videoDir}/${this.slug}.mp4");
  }

  Future<File> getFile() async {
    if (!this.exists) {
      if (!await this.download()) return null;
    }
    return this.file;
  }

  Future<bool> download() async {
    try {
      await Dio().download("${UIString.VideoSrcBase}${this.slug}.mp4?alt=media", this.file.path);
    }
    catch(e) {
      return false;
    }
    return true;
  }
}

class ColoredModBox extends StatelessWidget {
  final String text;
  final Color color;

  const ColoredModBox(this.text, this.color);

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
        decoration: BoxDecoration(color: color),
        child: Padding(
          padding: EdgeInsets.all(1),
          child: Text(text, textAlign: TextAlign.center, style: TextStyle(fontSize: 12, fontWeight: FontWeight.bold, color: Colors.white)),
        )
    );
  }
}

class SeqDisplay extends StatelessWidget {
  final KihonSeq seq;
  final bool active;
  final bool tappable;

  const SeqDisplay(this.seq, {this.active = false, this.tappable = false});

  List<Widget> _formattedSeq() {
    List<Widget> wline, cline = [];
    int i = 0;
    for (KihonID kid in this.seq.kihons) {
      final Kihon k = DefaultKihons[kid];
      final Movement mvt = this.seq.attributs[i].mvt;
      final Position pos = this.seq.attributs[i].pos;
      final Modifier mod = this.seq.attributs[i].mod;
      final Level lvl = this.seq.attributs[i].lvl;
      wline = [];
      if (i > 0) wline.add(Text(" ▶", style: TextStyle(color: Colors.red, fontSize: 12)));
      if (mvt != null) wline.add(ColoredModBox(Movements[mvt].tag, AttrTypes[AttrType.movement].color));
      if (pos != null) wline.add(ColoredModBox(Positions[pos].tag, AttrTypes[AttrType.position].color));
      if (mod != null) wline.add(ColoredModBox(Modifiers[mod].tag, AttrTypes[AttrType.mod].color));
      wline.add(KihonDisplay(k, active: this.active, popup: this.tappable ? KihonPopup(k) : null));
      if (lvl != null) wline.add(ColoredModBox(Levels[lvl].tag, AttrTypes[AttrType.level].color));

      cline.add(
          Wrap(
            spacing: 3,
            children: wline,
            crossAxisAlignment: WrapCrossAlignment.center,
          )
      );
      i++;
    }
    return cline;
  }

  @override
  Widget build(BuildContext context) {
    return Wrap(
      runSpacing: 3.0,
      alignment: WrapAlignment.start,
      crossAxisAlignment: WrapCrossAlignment.center,
      children: _formattedSeq(),
    );
  }
}

class KihonDisplay extends StatelessWidget {
  final Kihon kihon;
  final bool active;
  final KihonPopup popup;

  const KihonDisplay(this.kihon, {this.active = false, this.popup});

  Widget _kihonText() {
    return Text(
        this.kihon.format(),
        style: TextStyle(
          fontWeight: FontWeight.bold,
          color: active ? Colors.black : Colors.white,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    if (this.popup != null) {
      return GestureDetector(
        onTap: () async => await showDialog(context: context, builder: (context) => this.popup),
        child: _kihonText(),
      );
    } else {
      return _kihonText();
    }
  }
}

class KihonPopup extends StatelessWidget {
  final Kihon kihon;

  const KihonPopup(this.kihon);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        contentPadding: EdgeInsets.all(10),
        titlePadding: EdgeInsets.symmetric(vertical: 5),
        insetPadding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        title: Text(this.kihon.format(), textAlign: TextAlign.center),
        content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(this.kihon.trad, textAlign: TextAlign.center),
              Divider(),
              this.kihon.video ? DemoVideo(describeEnum(this.kihon.id)) : Text(UIString.NoVideo, textAlign: TextAlign.center, textScaleFactor: 0.8, style: TextStyle(color: Colors.grey)),
            ]
        )
    );
  }
}

class DemoVideo extends StatefulWidget {
  final String kihonSlug;

  DemoVideo(this.kihonSlug);

  @override
  _DemoVideoState createState() => _DemoVideoState();
}

class _DemoVideoState extends State<DemoVideo> {

  File videoFile;
  bool _loaded = false;
  bool _error = false;

  void _loadVideo() async {
    final VideoAsset asset = VideoAsset(widget.kihonSlug);
    videoFile = await asset.getFile();
    if (videoFile == null) _error = true;
    setState(() => _loaded = true );
  }

  @override
  void initState() {
    super.initState();
    _loadVideo();
  }

  @override
  Widget build(BuildContext context) {

    if (_loaded) {
      if (_error) return LoadingStatus(UIString.VideoDLErr, Icon(Icons.error_outline, color: Colors.red), true);
      else return VideoWidget(videoFile);
    } else return LoadingStatus(UIString.VideoDL);

  }
}

class VideoWidget extends StatefulWidget {
  final File videoFile;
  VideoWidget(this.videoFile);

  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {

  VideoPlayerController _video;

  @override
  void initState() {
    super.initState();
    _video = VideoPlayerController.file(widget.videoFile)
      ..setVolume(0)
      ..initialize().whenComplete(() {
        setState(() {});
        _video.play();
      });
  }

  @override
  void dispose() {
    super.dispose();
    _video.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (_video.value.initialized) {
      return Flexible(
        child: AspectRatio(
          aspectRatio: _video.value.aspectRatio,
          child: GestureDetector(
            onTap: () => _video.seekTo(Duration(seconds: 0)),
            child: VideoPlayer(_video),
          ),
        ),
      );
    } else
      return LoadingStatus(UIString.VideoLoading);
  }
}

class LoadingStatus extends StatelessWidget {
  final String text;
  final Widget header;
  final bool error;

  const LoadingStatus(this.text, [this.header = const CircularProgressIndicator(), this.error = false]);

  @override
  Widget build(BuildContext context) {
    return Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(height: 24, width: 24, margin: EdgeInsets.all(5), child: this.header),
          Text(this.text, textScaleFactor: 0.8, style: TextStyle(color: this.error ? Colors.red : Colors.grey))
        ]
    );
  }
}
