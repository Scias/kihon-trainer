/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/services.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:soundpool/soundpool.dart';
import 'package:device_apps/device_apps.dart';

import '../classes/kihons.dart';
import '../classes/config.dart';
import '../const/tts.dart';
import '../const/strings.dart';
import '../const/common.dart' show RunState, EpreuvePart, EpreuveParts;

final tts = Tts.instance;

// Progression épreuve en cours
class Progress {
  final EpreuvePart partid;
  final int seqid;

  const Progress(this.partid, this.seqid);
}

// Initialise & test Text To Speech
class Tts {

  bool ready = false;
  bool fttsworking = false;
  RunState state = RunState.stopped;
  final FlutterTts ftts = FlutterTts();
  final Soundpool spool = Soundpool(streamType: StreamType.music);
  int beepId;
  List<String> voicesFR = [];
  List<String> voicesJP = [];
  int progress;

  // Singleton
  static final Tts instance = Tts._();
  Tts._();

  Future<void> init() async {
    await this.initTTS();
    await this.initBeep();
  }

  // Workaround hang Flutter TTS
  Future<bool> _isFttsOk() async {
    if (!this.fttsworking) {
      this.fttsworking = true;
      final bool res = await this.ftts.isLanguageInstalled("fr-FR");
      this.fttsworking = false;
      return res;
    }
    else return false;
  }

  List<String> _getVoices(List voices, [String lang = "fr"]) {
    List<String> temp = [];

    final String locale = lang == "fr" ? "fr-FR" : "ja-JP";
    final String name = lang == "fr" ? "fr-fr-x-fr" : "ja-jp-x-ja";

    for (final Map v in voices) {
      if (v["locale"] == locale && v["name"].contains(name) && v["name"].contains("local")) {
        temp.add(v["name"]);
      }
    }

    return temp;
  }

  Future<bool> initTTS() async {
    // Verif Google TTS & voix FR installés sinon TTS désactivé

    if (!await DeviceApps.isAppInstalled('com.google.android.tts')) return false;
    if (!await Future.any([this._isFttsOk(), Future.delayed(Duration(seconds: 10), () => false)])) return false; // Workaround FTTS BUG

    // Sauvegarde voix présentes
    final List voices = await this.ftts.getVoices;

    // Voix FR (narrateur + tech)
    voicesFR = _getVoices(voices);
    if (voicesFR.isEmpty) return false;

    // Voix JP (tech)
    if (await this.ftts.isLanguageInstalled("ja-JP")) voicesJP = _getVoices(voices, "jp");

    this.voicesFR.sort();
    this.voicesJP.sort();

    // Workaround ftts stop() bug
    await this.ftts.awaitSpeakCompletion(true);

    // Tout est OK
    this.ready = true;

    // MaJ voix si config déjà faite
    if (globalConfig.ready) globalConfig.reloadVoices();

    return true;
  }

  Future<void> initBeep() async {
    this.beepId = await rootBundle.load(UIString.BeepSnd).then((ByteData data) {
      return this.spool.load(data);
    });
    await this.spool.setVolume(soundId: this.beepId, volume: 0.2);
  }

  Future<void> _wait(int delay) async {
    // Délai post-speak + bips
      for (int i = delay; i > 0 && this.state == RunState.running; i--) {
        if (delay > 4 && i < 4) await this.spool.play(this.beepId);
        await Future.delayed(Duration(seconds: 1));
    }
  }

  Future<void> speakline(String line, {bool tech = false, int delay = 0}) async {

    // PAUSE
    if (this.state == RunState.pausing || this.state == RunState.paused) {
      this.state = RunState.paused;
      while (this.state == RunState.paused) await Future.delayed(Duration(milliseconds: 500));
    }

    if (this.state == RunState.running) {
      if (!tech) await this.ftts.setVoice(globalConfig.get("voix_narr").value);
      else await this.ftts.setVoice(globalConfig.get("voix_tech").value);
      await this.ftts.speak(line);
      if (delay > 0) await this._wait(delay);
    }
  }

  Future<bool> test() async {
    try {
      await this.ftts.setVolume(0);
      await this.ftts.setVoice(globalConfig.get("voix_narr").value);
      await this.ftts.speak("Test");
      await this.ftts.setVoice(globalConfig.get("voix_tech").value);
      await this.ftts.speak("Test");
      await this.ftts.setVolume(1);
    }
    catch(e) {
      return false;
    }
    return true;
  }

  Stream<Progress> dictEpreuve(Epreuve epreuve) async* {
    this.state = RunState.running;
    await speakline(TTSString.begin, delay: TTSDelay.begin);

    for (final MapEntry<EpreuvePart, List<KihonSeq>> partSeqs in epreuve.seqs.entries) {
      final EpreuvePart partid = partSeqs.key;
      final List<KihonSeq> kihonSeqs = partSeqs.value;
      bool sub = false;
      await speakline(TTSString.part + (partid.index+1).toString());
      await speakline(EpreuveParts[partid].desc, delay: TTSDelay.part_start);
      for (int seqid = 0; seqid < kihonSeqs.length; seqid++) {
        final String techLang = globalConfig.get("voix_tech").lang;
        final int confDelay = globalConfig.get("delai").value;
        // TODO: BUGFIX CHANGEMENT DE VOIX LORS ÉPREUVE EN COURS
        final List<String> seqlines = kihonSeqs[seqid].toTts(techLang);
        int delay;
        double kd;
        if (sub) {
          if (partid == EpreuvePart.simple || partid == EpreuvePart.sequ) await speakline(TTSString.sub12);
          if (partid == EpreuvePart.surpl) await speakline(TTSString.part3);
          if (partid == EpreuvePart.multidir) await speakline(TTSString.sub4);
        }
        await speakline(TTSString.doseq);
        // Calcul délai execution séquence
        if (confDelay == 0) kd = 0.8;
        else if (confDelay == 1) kd = 1.2;
        else kd = 1.5;
        if (partid == EpreuvePart.simple || partid == EpreuvePart.sequ) delay = (kihonSeqs[seqid].count * kd * 3 + 3).round();
        else if (partid == EpreuvePart.surpl || partid == EpreuvePart.multidir) delay = (kihonSeqs[seqid].count * kd * 6 + 4).round();
        // Renvoi info progression au trainstate
        yield Progress(partid, seqid);
        //
        for (int i = 1; i <= 2; i++) {
          int j = 0;
          for (String s in seqlines) {
            if (j > 0 && seqlines.length > 0 && techLang == "fr") {
              j == 1 ? await speakline(TTSString.then, tech: true) : await speakline(TTSString.last, tech: true);
            }
            await speakline(s, delay: TTSDelay.seq_post, tech: true);
            j++;
          }
          if (i == 1) await speakline(TTSString.rep);
          else {
            if (globalConfig.get("traduc").value) {
              final List<String> tradlines = kihonSeqs[seqid].toTts("trad");
              await speakline(TTSString.trad);
              for (String s in tradlines) {
                await speakline(s, delay: TTSDelay.seq_post);
              }
            }
            await speakline(TTSString.start, delay: delay);
          }
        }
        sub = true;
      }
      await speakline(TTSString.end + (partid.index+1).toString(), delay: TTSDelay.part_post);
    }
    await speakline(TTSString.fin);
    this.state = RunState.stopped;
  }


  Future<void> stopEpreuve() async {
    this.state = RunState.stopping;
    while (this.state != RunState.stopped) await Future.delayed(Duration(seconds: 1));
  }

  Future<void> pauseEpreuve() async {
    this.state = RunState.pausing;
    while (this.state != RunState.paused) await Future.delayed(Duration(seconds: 1));
  }

  void resumeEpreuve() {
    this.state = RunState.running;
  }

}