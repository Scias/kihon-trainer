/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'dart:convert';
import 'package:flutter/foundation.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../const/options.dart';
import '../classes/tts.dart';
import '../const/tts.dart';

final globalConfig = Config.instance;

class Config {
  Map<String, Option> options = {};
  SharedPreferences save;
  bool usersave = false;
  bool ready = false;
  bool firstrun;

  // Singleton
  static final Config instance = Config._();
  Config._();

  // Charger options par défaut & sauvegardées
  void _loadDefault() {
    for (Map o in DefaultOptions) this.options[o["slug"]] = Option(o);
  }

  Future<void> load() async {
    this.save = await SharedPreferences.getInstance();
    final savedOptions = save.getString("options");
    this.firstrun = save.getBool("firstrun") ?? true;

    this._loadDefault();

    if (savedOptions != null) _loadSaved(savedOptions);
    ready = true;
  }

  // Future<void> reset() async {
  //   this._loadDefault();
  //   await this.save();
  // }

  // Restaurer options sauvegardées
  // TODO: à optimiser
  void _loadSaved(String savedOptions) {
    this.usersave = true;
    jsonDecode(savedOptions).forEach((slug, save) {
      if (this.options.containsKey(slug)) this.options[slug].restore(save);
    });
  }

  Future<void> saveConfig() async {
    Map<String, List> savedOptions = {};
    for (Option o in options.values) {
      if (o.type != OptionType.spacer) savedOptions[o.slug] = o.export();
    }
    await this.save.setString("options", jsonEncode(savedOptions));
  }

  Future<void> firstran() async {
    await save.setBool("firstrun", false);
    this.firstrun = false;
  }

  void reloadVoices() {
    this.options["voix_narr"].initVoice();
    this.options["voix_tech"].initVoice();
  }

  Option get(String slug) {
    if (options.containsKey(slug)) return this.options[slug];
    return null;
  }

}

class Option {

  String slug;
  String desc;
  String sub;
  String tooltip;
  OptionType type;
  List _choices;
  int rawValue;
  final ValueNotifier<List<String>> voices = ValueNotifier(null);

  bool actif;

  Option(Map o) {
    this.slug = o["slug"];
    this.desc = o["nom"];
    this.sub = o.containsKey("sub") ? o["sub"] : null;
    this.tooltip = o.containsKey("tooltip") ? o["tooltip"] : null;
    this.type = o["type"];
    if (this.type == OptionType.check) this.actif = o["actif"];
    if (this.type == OptionType.select || this.type == OptionType.input) this.rawValue = o["value"];
    if (this.type == OptionType.select) this._choices = o["choices"];
    if (this.type == OptionType.voice) this.initVoice();
  }

  void initVoice() {
    if (tts.ready) {
      int vIndex;
      if (this.slug == "voix_narr") {
        this.voices.value = tts.voicesFR;
        vIndex = this.voices.value.indexOf(DefaultNarrVoice);
      } else if (this.slug == "voix_tech") {
        this.voices.value = tts.voicesFR+tts.voicesJP;
        vIndex = this.voices.value.indexOf(DefaultTechVoice);
      }
      if (vIndex >= 0 ) this.rawValue = vIndex;
    }
    else this.voices.value = [];
  }

  List export() {
    return [this.actif, this.rawValue];
  }

  void restore(List save) {
    if (this.type == OptionType.check && save[0] != null) this.actif = save[0];
    else if (save[1] != null) {
      if (this.type == OptionType.select && save[1] < this._choices.length) this.rawValue = save[1];
      if (this.type == OptionType.voice && save[1] < this.voices.value.length) this.rawValue = save[1];
      if (this.type == OptionType.input) this.rawValue = save[1];
    }
  }

  void setTo(dynamic newValue) {
    this.type == OptionType.check ? this.actif = newValue : this.rawValue = newValue;
  }

  dynamic get value {
    switch(this.type) {
      case OptionType.check:
        return this.actif;
      case OptionType.select:
      case OptionType.input:
        return this.rawValue;
      case OptionType.voice:
        final String voice = this.voices.value[this.rawValue];
        final String locale = voice.contains(("fr-fr")) ? "fr-FR" : "ja-JP";
        return { "name" : voice, "locale" : locale };
      default:
        break;
    }
  }

  String get lang {
    if (this.type == OptionType.voice) {
      if (this.voices.value[this.rawValue].contains("fr")) return "fr";
      else return "jp";
    }
    else return null;
  }

  List<String> getChoices() {
    if (this.type == OptionType.voice) {
      return this.voices.value.map((String v) => VoiceIndex.containsKey(v) ? VoiceIndex[v] : v).toList();
    }
    else return this._choices;
  }

}