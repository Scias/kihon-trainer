/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'dart:math';
import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:path_provider/path_provider.dart';

import '../classes/config.dart';
import '../const/common.dart';
import '../const/kihons.dart';
import '../const/seqs.dart';

final globalIndex = KihonIndex.instance;

bool chance(int a, int b) {
  if (a <= 0 || b <= 0) return false;
  if (a >= b || Random().nextInt(b) <= a-1) return true;
  else return false;
}

int randomInt(int max) => Random().nextInt(max);

class Kihon {

  final KihonID id;
  final String name;
  final String jp;
  final String fr;
  final String trad;
  final KihonType type;
  final int diff;
  final bool nosuffix;
  final bool nomvt;
  final bool noniv;
  final bool nomod;
  final List excl;
  final List pref;
  final bool video;

  const Kihon({
    this.id,
    this.name,
    this.jp,
    this.fr,
    this.trad,
    this.type,
    this.diff,
    this.nosuffix = false,
    this.nomvt = false,
    this.noniv = false,
    this.nomod = false,
    this.excl,
    this.pref,
    this.video = false
  });

  String format([String lang]) {
    String suffix = "";
    if (!this.nosuffix) {
      if (lang == "fr" && KihonTypes[this.type].suffixFR != null) {
        suffix = " " + KihonTypes[this.type].suffixFR;
      }
      else if (lang == "jp" && KihonTypes[this.type].suffixJP != null) {
        suffix = " " + KihonTypes[this.type].suffixJP;
      }
      else suffix = " " + KihonTypes[this.type].suffix;
    }
    if (lang == "fr" && this.fr != null) return this.fr + suffix;
    else if (lang == "jp" && this.jp != null) return this.jp + suffix;
    else return this.name + suffix;
  }

}

//Attributs de Kihon (position/mouvements/gyaku-kizami/niveaux)
class Attribute {
  final Movement mvt;
  final Position pos;
  final Modifier mod;
  final Level lvl;

  const Attribute({this.mvt, this.pos, this.mod, this.lvl});
  
  bool equals(Attribute attr) {
    if (this.mvt == attr.mvt && this.pos == attr.pos && this.mod == attr.mod && this.lvl == attr.lvl) return true;
    else return false;
  }
}

// Séquence de Kihons ainsi que leurs attributs le cas échéant
// TODO: Optimiser : make const / genRandom dans classe/fonction séparée
class KihonSeq {
  List<KihonID> kihons = [];
  List<Attribute> attributs = [];
  final EpreuvePart partid;
  int diffScore = 0;

  // TODO: Optimiser : make static
  KihonSeq({this.partid, List<KihonID> kihonList, List<Movement> movList, List<Position> posList, List<Modifier> modList, List<Level> levelList }) {
    this.kihons = kihonList;
    for (int i = 0; i < kihonList.length; i++) {
      attributs.add(Attribute(
        mvt: movList != null ? movList[i] : null,
        pos: posList != null ? posList[i] : null,
        mod: modList != null ? modList[i] : null,
        lvl: levelList != null ? levelList[i] : null,
      ));
    }
  }

  KihonSeq.genRandom(List<KihonID> activeKihons, this.partid, KihonGenRules rules) { // Génerer séquence aléatoire
    int extra;
    KihonID prevKihon;
    Attribute prevAttr;
    int anti = 0;

    // P1 : 1 Kihon / P2 : >=2 / P3-4 : 1-3
    if (this.partid == EpreuvePart.simple) extra = 0;
    else if (this.partid == EpreuvePart.sequ) extra = 1 + randomInt(2);
    else extra = randomInt(3);

    for (int i = 0; i < 1 + extra; i++) {
      // PICK KIHON
      final KihonID kid = activeKihons[randomInt(activeKihons.length)];
      final Kihon kihon = DefaultKihons[kid];

      //// MODIFIERS
      // TODO: AJOUT POSE DEVANT/DERRIERE (PIEDS)
      // TODO: EQUILIBRER DEFS/PIEDS/POINGS (PARTIE 1)
      Movement mvt;
      Position pos;
      Modifier mod;
      Level lvl;
      // MOUVEMENT
      if (rules.withMvt && !kihon.nomvt && this.partid != EpreuvePart.simple && chance(1,4)) {
        mvt = Movement.values[randomInt(Movement.values.length)];
      }
      // POSITION
      if (rules.withPos && kihon.type != KihonType.kick && this.partid != EpreuvePart.multidir && chance(1,5)) {
        final pospool = Positions.keys.toList().where((s) => Positions[s].diff <= rules.diff + 1).toList();
        if (kihon.pref != null && chance(4,5)) { // Gestion Préférences (4/5 chances)
          final List pref = kihon.pref.toSet().intersection(pospool.toSet()).toList();
          if (pref.isNotEmpty) pos = pref[randomInt(pref.length)];
        }
        if (pos == null) pos = pospool[randomInt(pospool.length)];
      }
      // MODS (PRE)
      if (rules.withMod && !kihon.nomod && pos != Position.kib && pos != Position.shi && chance(1,3)) { // Si position != kiba ou shiko
        if (kihon.type == KihonType.def) mod = Modifier.gyaku; // Chance ajout gyaku
        else if (kihon.type == KihonType.kick) mod = Modifier.kizami; // Chance ajout kizami
      }
      // NIVEAUX (POST)
      if (rules.withNiv && !kihon.noniv && chance(1,5)) {
        final List nivpool = Level.values;
        lvl = nivpool[randomInt(nivpool.length)]; // Chance d'ajouter un niveau
      }
      // GERER EXCLUSIONS PARTICULIERES
      // TODO: À OPTIMISER
      if (kihon.excl != null) {
        if (kihon.excl.contains(mvt)) mvt = null;
        if (kihon.excl.contains(pos)) pos = null;
        if (kihon.excl.contains(mod)) mod = null;
        if (kihon.excl.contains(lvl)) lvl = null;
      }
      final Attribute attr = Attribute(mvt: mvt, pos: pos, mod: mod, lvl: lvl);
      ////

      // ANTI REPETITION
      if (i > 0 && anti < 3 && activeKihons.length > 1 && prevKihon == kid && prevAttr.equals(attr)) {
        anti++;
        i--;
        continue; // Recommence si kihon & mod identiques
      }
      prevKihon = kid;
      prevAttr = attr;
      ////

      // Ajout Kihon + Mods à liste si OK
      this.kihons.add(kid);
      this.attributs.add(attr);
      this.diffScore += kihon.diff;
    }
  }

  int get count => kihons.length;

  List<String> toTts(String lang) {
    List<String> ttslines = [];
    int i = 0;
    for (KihonID kid in this.kihons) {
      final Kihon k = DefaultKihons[kid];
      final Attribute attr = this.attributs[i];
      String mvt = "";
      String pos = "";
      String pre = "";
      String post = "";
      if (attr.mvt != null) {
        if (lang != "jp") mvt += "En faisant un ";
        if (lang == "trad") mvt += Movements[attr.mvt].desc + "... ";
        else {
          if (lang != 'jp' && Movements[attr.mvt].fr != null) mvt += Movements[attr.mvt].fr;
          else mvt += Movements[attr.mvt].name;
          mvt += " " + AttrTypes[AttrType.movement].suffix + "... ";
        }
      }
      if (attr.pos != null) {
        if (lang != "jp") pos += "En position ";
        if (lang == "trad") pos += Positions[attr.pos].desc + "... ";
        else pos += Positions[attr.pos].name + " " + AttrTypes[AttrType.position].suffix + "... ";
      }
      if (attr.mod != null) {
        if (lang == "trad") pre = " " + Modifiers[attr.mod].desc;
        else pre = Modifiers[attr.mod].name + " ";
      }
      if (attr.lvl != null) {
        post = " ";
        if (lang == "trad") post += Levels[attr.lvl].desc;
        else {
          if (lang == "fr" && Levels[attr.lvl].fr != null) post += Levels[attr.lvl].fr;
          else post += Levels[attr.lvl].name;
        }
      }

      if (lang == "fr") ttslines.add(mvt + pos + pre + k.format(lang) + post + ".");
      else if (lang == "jp") ttslines.add(mvt + pos + post + pre + k.format(lang) + ".");
      else ttslines.add(mvt + pos + k.trad + pre + post + ".");

      i++;
    }
    return ttslines;
  }
}

// Épreuve de Kihon, cad une liste de séquences de Kihon
class Epreuve {
  final KihonGenRules rules;
  Map<EpreuvePart, List<KihonSeq>> seqs = {};

  Epreuve(this.rules) {
    List<KihonID> currKihonPool;
    List<KihonSeq> currSeqPool;
    final Iterable<KihonID> kihonPool = globalIndex.getActiveKihons(rules.diff + 1);
    if (kihonPool.isEmpty && this.rules.mode != 1) return;
    if (globalIndex.allSeqs.isEmpty && this.rules.mode != 0) return;
    currKihonPool = List.from(kihonPool);
    rules.seqsPerPart.forEach((partid, nbSeqs) {
      this.seqs[partid] = [];
      if (nbSeqs > 0) {
        if (currKihonPool.length < kihonPool.length && this.rules.mode != 1) currKihonPool = List.from(kihonPool); // Anti répet p1
        if (this.rules.mode != 0) {
          currSeqPool = globalIndex.allSeqs.where((s) => s.partid == partid).toList();
          if (currSeqPool.isEmpty) return;
        }
        for (int seqid = 0; seqid < nbSeqs; seqid++) {
          KihonSeq seq;
          int currMode;

          // Génération Kihon aléatoire ou prédef selon option
          if (this.rules.mode == 2) currMode = randomInt(2);
          else currMode = this.rules.mode;
          if (currMode == 0) seq = KihonSeq.genRandom(currKihonPool, partid, rules);
          else seq = currSeqPool.elementAt(randomInt(currSeqPool.length));
          this.seqs[partid].add(seq);

          // Anti repet p1
          if (partid == EpreuvePart.simple && currMode != 1) {
            currKihonPool.remove(this.seqs[partid].last.kihons.first);
            if (currKihonPool.isEmpty) currKihonPool = List.from(kihonPool);
          }
          // Anti repet seqs
          if (currMode != 0) {
            currSeqPool.remove(seq);
            if (currSeqPool.isEmpty) currSeqPool = globalIndex.allSeqs.where((s) => s.partid == partid).toList();
          }
        }
      }
    });
  }

  bool get isEmpty => seqs.isEmpty;

}

// Répertoire liste des Kihons et séquences de l'application et leut statut actif ou non
// Charge/Sauve paramètres kihon/séquences sauvegardées
class KihonIndex {
  Map<KihonID, bool> kihonList = {};
  Map<KihonSeq, bool> seqList = {};
  final List<KihonSeq> builtinSeqs = DefaultSeqs;
  final SharedPreferences save = globalConfig.save;
  //List<KihonSeq> userSeqs = [];
  List exclKihons = [];
  String videoDir;

  // Make singleton
  static final KihonIndex instance = KihonIndex._();
  KihonIndex._();

  void _loadExcl() {
    final excl = this.save.getString("exclKihons");
    if (excl != null) exclKihons = jsonDecode(excl);
  }

  Future<void> _saveExcl() async {
    await this.save.setString("exclKihons", jsonEncode(exclKihons));
  }

  Future<void> load() async {
    videoDir = (await getApplicationDocumentsDirectory()).path;
    _loadExcl();
    for (KihonID kid in DefaultKihons.keys) {
      if (exclKihons != null) kihonList[kid] = exclKihons.contains(kid.index) ? false : true;
      else kihonList[kid] = true;
    }
  }

  Future<void> toggle(KihonID kid, bool value) async {
    kihonList[kid] = value;
    value ? exclKihons.remove(kid.index) : exclKihons.add(kid.index);
    await _saveExcl();
  }

  Iterable<KihonID> getActiveKihons([int maxdiff = 4]) =>
      kihonList.keys.where((kid) => kihonList[kid] && DefaultKihons[kid].diff <= maxdiff);

  List<KihonSeq> get allSeqs => builtinSeqs;

  Future<void> reset() async {
    kihonList.updateAll((kid, val) => true);
    exclKihons.clear();
    await _saveExcl();
  }


}

// Set de règles pour la génération d'épreuve
class KihonGenRules {
  final Map<EpreuvePart, int> seqsPerPart;
  final int mode;
  final int diff;
  final bool withMvt;
  final bool withPos;
  final bool withMod;
  final bool withNiv;

  const KihonGenRules({
    this.seqsPerPart,
    this.mode,
    this.diff,
    this.withMvt,
    this.withPos,
    this.withMod,
    this.withNiv
  });
}