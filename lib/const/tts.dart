/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

class TTSString {
  TTSString._();
  static const String begin = "L'épreuve du Ki hon va commencer dans dix secondes...";
  static const String part = "Partie ";
  static const String end = "Fin de la partie ";
  static const String part1 = "Technique simple sur trois pas.";
  static const String part2 = "Enchaînement de techniques sur trois pas.";
  static const String sub12 = "Sur trois pas... ";
  static const String part3 = "Sur place; trois fois à gauche puis à droite... ";
  static const String part4 = "En multidirectionnel. À gauche puis à droite... ";
  static const String sub4 = "En multidirectionnel...";
  static const String doseq = "Effectuer : ";
  static const String start = "Hajimé !";
  static const String start_jp = "始め!";
  static const String rep = "Encore une fois : ";
  static const String trad = "Traduction : ";
  static const String then = "Ensuite : ";
  static const String last = "Et enfin : ";
  static const String fin = "L'épreuve du Ki hon est terminée.";
}

class TTSDelay {
  TTSDelay._();
  static const int begin = 10;
  static const int part_start = 2;
  static const int part_post = 2;
  static const int seq_post = 1;
}

// Liste des voix Google TTS

const Map<String, String> VoiceIndex = {
  "fr-fr-x-fra-local" : "FR: Femme A",
  "fr-fr-x-frb-local" : "FR: Homme A",
  "fr-fr-x-frc-local" : "FR: Femme B",
  "fr-fr-x-frd-local" : "FR: Homme B",
  "ja-jp-x-jab-local" : "JP: Femme A",
  "ja-jp-x-jac-local" : "JP: Homme A",
  "ja-jp-x-jad-local" : "JP: Homme B",
};

// Voix par défaut si non définies par l'user

const String DefaultNarrVoice = "fr-fr-x-frb-local";
const String DefaultTechVoice = "fr-fr-x-frc-local";