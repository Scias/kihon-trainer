/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

enum OptionType {
  select,
  input,
  check,
  spacer,
  voice,
}

const List<String> Diff = [
  "1 - Débutant",
  "2 - Intermédiaire",
  "3 - Avancé",
  "4 - Expert"
];

const List<String> Delais = [
  "Court",
  "Normal",
  "Long"
];

const List<String> Modes = [
  "Aléatoire seul",
  "Prédéfini seul",
  "Hybride"
];

const List<Map> DefaultOptions = [
  { "type" : OptionType.spacer, "slug" : "spacer0" , "sub" : "Les options marquées ★ n'affecteront que les prochaines générations d'épreuves." },
  { "type" : OptionType.select, "slug" : "diff" , "nom" : "★ Difficulté" , "choices" : Diff, "value" : 1, "sub" : "Difficulté maximum des techniques générées aléatoirement." },
  { "type" : OptionType.select, "slug" : "mode" , "nom" : "★ Mode" , "choices" : Modes, "value" : 2, "sub" : "Définit la méthode de génération des techniques.",
    "tooltip" : "• Mode Aléatoire seul :\nL'épreuve sera uniquement composée de séquences de Kihon générées aléatoirement."
        "\n\n• Mode Prédéfini seul:\nL'épreuve sera uniquement composée de séquences déjà faites (voir onglet Techniques)."
        "\n\n• Mode Hybride:\nL'épreuve sera composée d'un mix de séquences aléatoires et prédéfinies."},
  { "type" : OptionType.select, "slug" : "delai" , "nom" : "Délai d'éxecution" , "sub" : "Délai d'attente pour l'éxecution d'une séquence de techniques avant de passer à la suivante.", "choices" : Delais, "value" : 1 },
  { "type" : OptionType.check, "slug" : "traduc" , "nom" : "Traduction en Français" , "sub" : "L'annonceur traduira les techniques demandées avant la phase d'execution." , "actif" : false },
  { "type" : OptionType.check, "slug" : "nospoil", "nom" : "★ Affichage progressif", "sub" : "Révèle l'épreuve progressivement pendant la dictée vocale", "actif" : false},
  { "type" : OptionType.spacer, "slug" : "spacer1" , "sub" : "Réglage des voix utilisées lors de la dictée de l'épreuve." },
  { "type" : OptionType.voice, "slug" : "voix_narr" , "nom" : "Voix annonceur" },
  { "type" : OptionType.voice, "slug" : "voix_tech" , "nom" : "Voix techniques" },
  { "type" : OptionType.spacer, "slug" : "spacer2" , "sub" : "Chaque technique générée a des chances de se voir des attributs imposés, sauf si désactivé ci-dessous.\nCela n'affecte que les techniques générées aléatoirement." },
  { "type" : OptionType.check, "slug" : "mvt" , "nom" : "★ Déplacements" , "sub" : "Ajout des déplacements aux techniques (par ex. pas chassé, en reculant...)" , "actif" : true },
  { "type" : OptionType.check, "slug" : "pos" , "nom" : "★ Positions" , "sub" : "Ajout des positions aux techniques (par ex. Kokutsu, Kiba...)" , "actif" : true },
  { "type" : OptionType.check, "slug" : "mod" , "nom" : "★ Gyaku/Kizami" , "sub" : "Ajout de Gyaku (défenses, poings...) et Kizami (pieds)" , "actif" : true },
  { "type" : OptionType.check, "slug" : "niv" , "nom" : "★ Niveaux d'attaque" , "sub" : "Ajout des hauteurs aux techniques (Jodan/Shudan/Gedan)" , "actif" : true },
  { "type" : OptionType.spacer, "slug" : "spacer3" , "sub" : "Nombre de répétitions pour chaque partie de l'épreuve. \nUne valeur de 0 désactive la partie concernée." },
  { "type" : OptionType.input, "slug" : "simple" , "nom" : "★ Technique simple (3 pas)" , "value" : 9 },
  { "type" : OptionType.input, "slug" : "sequ" , "nom" : "★ Enchaînement (3 pas)" , "value" : 4 },
  { "type" : OptionType.input, "slug" : "surpl" , "nom" : "★ Sur place (gauche/droite)" , "value" : 5 },
  { "type" : OptionType.input, "slug" : "multidir" , "nom" : "★ Multidirectionnel (gauche/droite)" , "value" : 3 },
];