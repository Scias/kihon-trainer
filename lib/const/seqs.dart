/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. EpreuvePart.surpl.EpreuvePart.simple. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/EpreuvePart.surpl.EpreuvePart.simple/. */

import '../const/kihons.dart';
import '../classes/kihons.dart';
import 'common.dart' show Movement, Position, Modifier, Level, EpreuvePart;

// SÉQUENCES PRÉENREGISTRÉES

// TODO make const
final List<KihonSeq> DefaultSeqs = [
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.sotouke],
    posList: [Position.zen],
    modList: [Modifier.gyaku],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.gedanb],
    posList: [Position.kib],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.mawashigeri],
    posList: [Position.fuu],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.uchirogeri],
    posList: [Position.fuu],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.maegeri],
    posList: [Position.zen],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.sukuiuke],
    posList: [Position.fuu],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.uraken],
    posList: [Position.zen],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.tatezuki],
    posList: [Position.zen],
    modList: [Modifier.gyaku],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.haitouchi],
    posList: [Position.fuu],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.sotouke],
    posList: [Position.kok],
  ),
  KihonSeq(
    partid: EpreuvePart.simple,
    kihonList: [KihonID.shutouke],
    posList: [Position.kok],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.jodanage, KihonID.maegeri],
    movList: [Movement.dea, null ],
    modList: [null,Modifier.kizami],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.gedanb, KihonID.gyakuzuki, KihonID.kizamizuki],
    movList: [Movement.dea,null,null],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.sukuiuke, KihonID.yokogerikek, KihonID.gyakuzuki],
    posList: [Position.kib, null, null],
    movList: [Movement.ayu, Movement.tsu, null],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.kizamizuki, KihonID.gyakuzuki, KihonID.mawashigeri],
    posList: [Position.fuu, null, null],
    movList: [Movement.yor, null, null],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.gyakuzuki, KihonID.uraken, KihonID.mawashigeri],
    movList: [Movement.dea, Movement.iki, Movement.tsu],
    modList: [ null, null, Modifier.kizami],
    levelList: [ null , Level.jodan , null ],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.maegeri, KihonID.gedanb, KihonID.uraken],
    movList: [Movement.dea, null, Movement.yor],
    posList: [Position.zen, null, null],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.uraken, KihonID.shutouke, KihonID.maegeri],
    movList: [Movement.dea, null, null],
    posList: [Position.kib, Position.kok, null],
    modList: [ null, null, Modifier.kizami],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.gedanb, KihonID.gyakuzuki, KihonID.shutouke],
    movList: [Movement.iki, null , null ],
    posList: [Position.zen , null , Position.kok],
    levelList: [ null , Level.jodan , null ],
  ),
  KihonSeq(
    partid: EpreuvePart.sequ,
    kihonList: [KihonID.oizuki,KihonID.sukuiuke,KihonID.yokogerikek],
    movList: [null,null,Movement.tsu],
    posList: [Position.fuu,Position.kok,null],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.jodanage,KihonID.mawashigeri],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.kizamizuki,KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.gyakuzuki,KihonID.maegeri],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.uraken,KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.mawashigeri,KihonID.uraken],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.yokogerikea,KihonID.osaeuke,KihonID.haitouchi],
    modList: [null,null,Modifier.gyaku],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.gyakuzuki,KihonID.sotouke,KihonID.maegeri],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.sotouke,KihonID.jodanage,KihonID.gedanb],
    levelList: [Level.jodan,null,null],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.maegeri,KihonID.jodanage,KihonID.haitouchi],
    modList: [ null, null, Modifier.gyaku],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.shutouke,KihonID.gyakuzuki,KihonID.kizamizuki],
  ),
  KihonSeq(
    partid: EpreuvePart.surpl,
    kihonList: [KihonID.sotouke, KihonID.mawashigeri],
    movList: [Movement.ikt, null ],
    levelList: [ null , Level.jodan ],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.yokogerikek,KihonID.oizuki,KihonID.gyakuzuki],
    levelList: [null,Level.shudan,null],
    modList: [Modifier.kizami,null,null],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.kizamizuki,KihonID.gyakuzuki,KihonID.mawashigeri],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.gyakuzuki,KihonID.kizamizuki,KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.maegeri,KihonID.shutouchi,KihonID.haitouchi],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.uchiuke,KihonID.gyakuzuki,KihonID.uchirogeri],
    movList: [Movement.ayu,null,null],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.uraken,KihonID.sotouke,KihonID.mawashigeri],
    modList: [null,null,Modifier.kizami],
    movList: [Movement.yor,Movement.iki,null],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.uraken,KihonID.gyakuzuki,KihonID.mawashigeri],
    modList: [null,null,Modifier.kizami],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.gyakuzuki,KihonID.sotouke,KihonID.maegeri],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.maegeri,KihonID.uchiuke,KihonID.kizamizuki],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.mawashigeri,KihonID.gedanb,KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.mawashigeri,KihonID.kizamizuki,KihonID.gyakuzuki],
  ),
  KihonSeq(
    partid: EpreuvePart.multidir,
    kihonList: [KihonID.sotouke, KihonID.gyakuzuki,KihonID.yokogerikea],
    movList: [Movement.iki,Movement.yor,Movement.tsu],
  ),
];