/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';

enum RunState { stopping, stopped, starting, running, pausing, paused }

class CommonStruct {
  final String name;
  final String desc;
  final String fr;
  final String suffix;
  final String suffixJP;
  final String suffixFR;
  final String tag;
  final Color color;
  final int diff;
  const CommonStruct({this.name, this.desc, this.fr, this.suffix, this.suffixJP, this.suffixFR, this.tag, this.color, this.diff});
}

// Types Kihon

enum KihonType { def, hand, kick }

const Map<KihonType, CommonStruct> KihonTypes = {
  KihonType.def : CommonStruct(name: "Blocages, défenses", suffix: "Uke" , suffixJP : "受け", suffixFR: "Ouké"),
  KihonType.hand : CommonStruct(name: "Poings, mains ouvertes, coudes", suffix: "Tsuki", suffixJP: "突き"),
  KihonType.kick : CommonStruct(name: "Pieds, genoux, balayages", suffix: "Geri" , suffixJP : "蹴り", suffixFR: "Guéri"),
};

// Types Attributs

enum AttrType { movement, position, mod, level }

const Map<AttrType, CommonStruct> AttrTypes = {
  AttrType.movement : CommonStruct(name: "Déplacements", suffix: "Achi" , suffixJP: "足", color : Colors.blueAccent),
  AttrType.position : CommonStruct(name: "Positions", suffix: "Dachi" , suffixJP: "立ち", color : Colors.purple),
  AttrType.mod : CommonStruct(name: "Attributs", color : Colors.green),
  AttrType.level : CommonStruct(name: "Niveaux", color : Colors.deepOrange),
};

// Déplacements

enum Movement { dea, ayu, tsu, yor, iki, ikt, iky }

const Map<Movement, CommonStruct> Movements = {
  Movement.dea : CommonStruct(tag: "DEA", name: "De", desc: "pas en avant", fr : "Dé" ),
  Movement.ayu : CommonStruct(tag: "AYU", name: "Ayumi" , desc: "pas en avant" ),
  Movement.tsu : CommonStruct(tag: "TSU", name: "Tsugi" , desc: "pas chassé" ),
  Movement.yor : CommonStruct(tag: "YOR", name: "Yori", desc: "pas trainé" ),
  Movement.iki : CommonStruct(tag: "IKI", name: "Iki" , desc: "pas en arrière" ),
  Movement.ikt : CommonStruct(tag: "IKT", name: "Iki Tsugi" , desc: "pas chassé en arrière" ),
  Movement.iky : CommonStruct(tag: "IKY", name: "Iki Yori" , desc: "pas trainé en arrière" ),
};

// Positions

enum Position { zen, kok, kib, fuu, shi, nek, kos, san, han, tsu }

const Map<Position, CommonStruct> Positions = {
  Position.zen : CommonStruct(tag : "ZEN", name: "Zenkutsu", desc: "en fente avant", diff: 1 ),
  Position.kok : CommonStruct(tag : "KOK", name: "Kokutsu", desc: "en fente arrière", diff: 1 ),
  Position.kib : CommonStruct(tag : "KIB", name: "Kiba", desc: "du cavalier", diff: 1 ),
  Position.fuu : CommonStruct(tag : "FUU", name: "Fuudo", desc: "équilibrée de combat", diff: 1 ),
  // NIV 2
  Position.shi : CommonStruct(tag : "SHI", name: "Shiko", desc: "du sumo", diff: 2 ),
  // NIV 3
  Position.nek : CommonStruct(tag : "NEK", name: "Neko Ashi", desc: "du chat", diff: 3 ),
  Position.kos : CommonStruct(tag : "KOS", name: "Kosa", desc: "pieds croisés", diff: 3 ),
  // NIV 4
  Position.san : CommonStruct(tag : "SAN", name: "Sanchin", desc: "du sablier", diff: 4 ),
  Position.han : CommonStruct(tag : "HAN", name: "Hangetsu", desc: "du sablier élargie", diff: 4 ),
  Position.tsu : CommonStruct(tag : "TSU", name: "Tsuru Ashi", desc: "debout sur une jambe", diff: 4 ),
};

// Modifiers

enum Modifier { gyaku, kizami }

const Map<Modifier, CommonStruct> Modifiers = {
  Modifier.gyaku : CommonStruct(tag : "GYA", name : "Gyaku", desc : "du bras arrière" ),
  Modifier.kizami : CommonStruct(tag : "KIZ", name: "Kizami", desc: "du pied avant" ),
};

// Niveaux

enum Level { gedan, shudan, jodan }

const Map<Level, CommonStruct> Levels = {
  Level.gedan : CommonStruct(tag : "GED", name : "Gedan", desc : "au niveau bas", fr : "Guédanne" ),
  Level.shudan : CommonStruct(tag : "SHU", name : "Shudan", desc : "au niveau du plexus", fr : "Shoudanne" ),
  Level.jodan : CommonStruct(tag : "JOD", name : "Jodan", desc : "au niveau du visage", fr : "Jodanne" ),
};

//// PARTIES EPREUVE

enum EpreuvePart { simple, sequ, surpl, multidir }

const Map<EpreuvePart, CommonStruct> EpreuveParts = {
  EpreuvePart.simple : CommonStruct(name : "Simple (3 pas)", desc : "Technique simple sur 3 pas" ),
  EpreuvePart.sequ : CommonStruct(name : "Enchaînement (3 pas)", desc : "Enchaînement sur 3 pas" ),
  EpreuvePart.surpl : CommonStruct(name : "Sur place", desc : "Sur place avec ou sans sursaut, à droite puis à gauche" ),
  EpreuvePart.multidir : CommonStruct(name : "Multidirectionnel", desc : "En multidirectionnel, à droite puis à gauche" ),
};