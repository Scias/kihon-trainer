/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

import 'common.dart' show KihonType, Movement, Position, Level;
import '../classes/kihons.dart' show Kihon;

//
// //// LISTE DE TOUTES LES TECHNIQUES INDIVIDUELLES
//
enum KihonID {
  gedanb,
  jodanage,
  uchiuke,
  sotouke,
  shutouke,
  moroteuke,
  shutob,
  otoshiuke,
  haitouke,
  heikouke,
  osaeuke,
  nagashi,
  haishuuke,
  empiuke,
  kakiuke,
  teishouke,
  jujiuke,
  sukuiuke,
  teishootouke,
  moroteteisho,
  kokenuke,
  oizuki,
  maetezuki,
  kizamizuki,
  gyakuzuki,
  uraken,
  nukite,
  tatezuki,
  urazuki,
  mawashizuki,
  shutouchi,
  empiuchi,
  tetsui,
  haitouchi,
  teishouchi,
  morotezuki,
  agezuki,
  yamazuki,
  sanbonzuki,
  kagizuki,
  haishuuchi,
  kokenuchi,
  maegeri,
  yokogerikek,
  mawashigeri,
  fumikomi,
  uchirogeri,
  yokogerikea,
  namigaeshi,
  hizageri,
  fumikiri,
  uramawashi,
  mikazukigeri,
  tobigeri,
  ashibarai,
  otoshigeri,
  nidangeri,
  uramikazuki,
  uchirobarai,
  kakatogeri
}

const Map<KihonID, Kihon> DefaultKihons = {
  KihonID.gedanb: Kihon(
    id: KihonID.gedanb,
    name: "Gedan Barai",
    fr: "Guédanne Baraille",
    jp: "下段払い",
    trad: "Balayage au niveau bas avec l'avant bras",
    type: KihonType.def,
    diff: 1,
    nosuffix: true,
    noniv: true,
    video: true,
  ),
  KihonID.jodanage: Kihon(
    id: KihonID.jodanage,
    name: "Jodan Age",
    fr: "Jodanne Agué",
    jp: "上段上げ",
    trad: "Blocage de bas en haut avec l'avant bras",
    type: KihonType.def,
    diff: 1,
    noniv: true,
  ),
  KihonID.uchiuke: Kihon(
    id: KihonID.uchiuke,
    name: "Uchi",
    jp: "打ち",
    trad: "Blocage de l'intérieur vers l'extérieur avec l'avant bras",
    type: KihonType.def,
    diff: 1,
    excl: [Level.gedan],
  ),
  KihonID.sotouke: Kihon(
    id: KihonID.sotouke,
    name: "Soto",
    //jp: "外",
    trad: "Blocage de l'extérieur vers l'intérieur avec l'avant bras",
    type: KihonType.def,
    diff: 1,
    excl: [Level.gedan],
  ),
  KihonID.shutouke: Kihon(
    id: KihonID.shutouke,
    name: "Shuto",
    //jp: "手刀",
    trad: "Défense avec le tranchant extérieur de la main",
    type: KihonType.def,
    diff: 1,
    pref: [Position.kok, Position.nek],
    video: true,
    nomod: true,
  ),

  // NIV 2

  KihonID.moroteuke: Kihon(
    id: KihonID.moroteuke,
    name: "Morote",
    jp: "諸手",
    trad: "Blocage renforcé avec les deux bras",
    type: KihonType.def,
    diff: 2,
    excl: [Level.gedan],
    nomod: true,
  ),
  KihonID.shutob: Kihon(
    id: KihonID.shutob,
    name: "Shuto Barai",
    //jp: "手刀払い",
    trad: "Blocage au niveau bas avec le tranchant de la main",
    type: KihonType.def,
    diff: 2,
    nosuffix: true,
    noniv: true,
  ),
  KihonID.otoshiuke: Kihon(
    id: KihonID.otoshiuke,
    name: "Otoshi",
    jp: "落とし",
    trad: "Blocage de haut en bas avec l'avant bras",
    type: KihonType.def,
    diff: 2,
    excl: [Level.jodan],
  ),

  // NIV 3

  KihonID.haitouke: Kihon(
    id: KihonID.haitouke,
    name: "Haito",
    fr: "Haïto",
    //jp: "背刀",
    trad: "Blocage avec la partie interne de la main",
    type: KihonType.def,
    diff: 3,
  ),
  KihonID.heikouke: Kihon(
    id: KihonID.heikouke,
    name: "Heiko",
    fr: "Héïko",
    trad: "Défense double avec les deux bras parallèles",
    type: KihonType.def,
    diff: 3,
    nomod: true,
  ),
  KihonID.osaeuke: Kihon(
    id: KihonID.osaeuke,
    name: "Osae",
    fr: "Ossa",
    trad: "Défense par déviation ou immobilisation en main ouverte",
    type: KihonType.def,
    diff: 3,
  ),
  KihonID.nagashi: Kihon(
    id: KihonID.nagashi,
    name: "Nagashi",
    trad: "Défense brossée en acompagnant l'attaque",
    type: KihonType.def,
    diff: 3,
  ),
  KihonID.haishuuke: Kihon(
    id: KihonID.haishuuke,
    name: "Haishu",
    fr: "Haïshou",
    //jp: "背手",
    trad: "Blocage avec le dos de la main",
    type: KihonType.def,
    diff: 3,
  ),
  KihonID.empiuke: Kihon(
    id: KihonID.empiuke,
    name: "Empi",
    jp: "猿臂",
    trad: "Blocage avec le coude",
    type: KihonType.def,
    diff: 3,
    nomod: true,
  ),
  KihonID.kakiuke: Kihon(
    id: KihonID.kakiuke,
    name: "Kakiwake",
    fr: "Kakiwaké",
    jp: "掻き分け",
    trad: "Dégagement avec les 2 bras",
    type: KihonType.def,
    diff: 3,
    nomod: true,
  ),
  KihonID.teishouke: Kihon(
    id: KihonID.teishouke,
    name: "Teisho",
    //jp: "底所",
    trad: "Blocage avec la paume de la main",
    type: KihonType.def,
    diff: 3,
  ),
  KihonID.jujiuke: Kihon(
    id: KihonID.jujiuke,
    name: "Juji",
    trad: "Blocage avec les bras en croix",
    type: KihonType.def,
    diff: 3,
    nomod: true,
  ),

  // NIV 4

  KihonID.sukuiuke: Kihon(
    id: KihonID.sukuiuke,
    name: "Sukui",
    //jp: "掬",
    trad: "Blocage en cuillère",
    type: KihonType.def,
    diff: 4,
    excl: [Level.jodan],
  ),
  KihonID.teishootouke: Kihon(
    id: KihonID.teishootouke,
    name: "Teisho Otoshi",
    //jp: "底所",
    trad: "Blocage de haut en bas avec la paume de la main",
    type: KihonType.def,
    diff: 4,
  ),
  KihonID.moroteteisho: Kihon(
    id: KihonID.moroteteisho,
    name: "Morote Teisho",
    //jp: "底所",
    trad: "Blocage de part et d'autre du corps avec la paume de la main",
    type: KihonType.def,
    diff: 4,
    nomod: true,
  ),
  KihonID.kokenuke: Kihon(
    id: KihonID.kokenuke,
    name: "Koken",
    trad: "Défense avec le poignet",
    type: KihonType.def,
    diff: 4,
  ),

  //// POINGS

  // NIV 1

  KihonID.oizuki: Kihon(
    id: KihonID.oizuki,
    name: "Oi",
    jp: "追い",
    fr: "Hoy",
    trad: "Coup de poing du bras avant en avançant",
    type: KihonType.hand,
    diff: 1,
    nomvt: true,
    nomod: true,
  ),
  KihonID.maetezuki: Kihon(
    id: KihonID.maetezuki,
    name: "Maete",
    fr: "Maété",
    //jp: "前手",
    trad: "Coup de poing du bras avant, épaules effacées",
    type: KihonType.hand,
    diff: 1,
    nomod: true,
    excl: [Movement.dea, Movement.ayu],
  ),
  KihonID.kizamizuki: Kihon(
    id: KihonID.kizamizuki,
    name: "Kizami",
    jp: "刻み",
    trad: "Coup de poing du bras avant",
    type: KihonType.hand,
    diff: 1,
    nomod: true,
    video: true,
    excl: [Movement.dea, Movement.ayu],
  ),
  KihonID.gyakuzuki: Kihon(
      id: KihonID.gyakuzuki,
      name: "Gyaku",
      trad: "Coup de poing du bras arrière",
      type: KihonType.hand,
      diff: 1,
      nomod: true,
      video: true,
      excl: [Position.kib, Position.shi]),
  KihonID.uraken: Kihon(
    id: KihonID.uraken,
    name: "Uraken Uchi",
    fr: "Ura Ken Outchi",
    jp: "裏拳打ち",
    trad: "Coup avec le revers du poing",
    type: KihonType.hand,
    diff: 1,
    nosuffix: true,
  ),

  // NIV 2

  KihonID.nukite: Kihon(
    id: KihonID.nukite,
    name: "Nukite",
    fr: "Nukité",
    jp: "貫手",
    trad: "Pique avec les doigts",
    type: KihonType.hand,
    diff: 2,
  ),
  KihonID.tatezuki: Kihon(
    id: KihonID.tatezuki,
    name: "Tate",
    fr: "Taté",
    jp: "縦",
    trad: "Coup de poing court, vertical",
    type: KihonType.hand,
    diff: 2,
  ),
  KihonID.urazuki: Kihon(
    id: KihonID.urazuki,
    name: "Ura",
    jp: "裏",
    trad: "Coup de poing en crochet",
    type: KihonType.hand,
    diff: 2,
  ),
  KihonID.mawashizuki: Kihon(
    id: KihonID.mawashizuki,
    name: "Mawashi",
    trad: "Coup de poing circulaire",
    type: KihonType.hand,
    diff: 2,
  ),
  KihonID.shutouchi: Kihon(
    id: KihonID.shutouchi,
    name: "Shuto Uchi",
    //jp: "手刀打ち",
    trad: "Attaque avec le tranchant externe de la main",
    type: KihonType.hand,
    diff: 2,
    nosuffix: true,
  ),
  KihonID.empiuchi: Kihon(
    id: KihonID.empiuchi,
    name: "Empi Uchi",
    jp: "猿臂打ち",
    trad: "Attaque avec le coude",
    type: KihonType.hand,
    diff: 2,
    nosuffix: true,
    nomod: true,
    video: true,
  ),
  KihonID.tetsui: Kihon(
    id: KihonID.tetsui,
    name: "Tetsui Uchi",
    fr: "Tetsui Outchi",
    jp: "鉄槌打ち",
    trad: "Coup de marteau du poing",
    type: KihonType.hand,
    diff: 2,
    nosuffix: true,
    video: true,
  ),
  KihonID.haitouchi: Kihon(
    id: KihonID.haitouchi,
    name: "Haito Uchi",
    fr: "Haïto Outchi",
    //jp: "背刀打ち",
    trad: "Frappe de la main avec le tranchant intérieur",
    type: KihonType.hand,
    diff: 2,
    nosuffix: true,
  ),

  // NIV 3

  KihonID.teishouchi: Kihon(
    id: KihonID.teishouchi,
    name: "Teisho Uchi",
    fr: "Teisho Outchi",
    //jp: "底内",
    trad: "Coup avec la paume de la main",
    type: KihonType.hand,
    diff: 3,
    nosuffix: true,
  ),
  KihonID.morotezuki: Kihon(
    id: KihonID.morotezuki,
    name: "Morote",
    // jp: "双手"
    trad: "Coup de poing double",
    type: KihonType.hand,
    diff: 3,
    nomod: true,
  ),
  KihonID.agezuki: Kihon(
    id: KihonID.agezuki,
    name: "Age",
    fr: "Agué",
    jp: "上げ",
    trad: "Coup de poing remontant",
    type: KihonType.hand,
    diff: 3,
  ),
  KihonID.yamazuki: Kihon(
    id: KihonID.yamazuki,
    name: "Yama",
    trad: "Double coup de poing, simultanément en haut et en bas",
    type: KihonType.hand,
    diff: 3,
    noniv: true,
    nomod: true,
  ),
  KihonID.sanbonzuki: Kihon(
    id: KihonID.sanbonzuki,
    name: "Sanbon",
    trad: "Coup de poing répété trois fois",
    type: KihonType.hand,
    diff: 3,
    nomod: true,
    noniv: true,
    video: true,
  ),
  KihonID.kagizuki: Kihon(
    id: KihonID.kagizuki,
    name: "Kagi",
    fr: "Kagui",
    trad: "Coup de poing parallèle en crochet",
    type: KihonType.hand,
    diff: 3,
  ),
  KihonID.haishuuchi: Kihon(
    id: KihonID.haishuuchi,
    name: "Haishu Uchi",
    fr: "Haïshou Outchi",
    trad: "Attaque avec le dos de la main",
    type: KihonType.hand,
    diff: 3,
    nosuffix: true,
  ),
  // NIV 4

  KihonID.kokenuchi: Kihon(
    id: KihonID.kokenuchi,
    name: "Koken Uchi",
    fr: "Koken Outchi",
    trad: "Attaque avec le dessus du poignet",
    type: KihonType.hand,
    diff: 4,
    nosuffix: true,
  ),

  //// PIEDS

  // NIV 1

  KihonID.maegeri: Kihon(
    id: KihonID.maegeri,
    name: "Mae",
    fr: "Maé",
    jp: "前",
    trad: "Coup de pied droit",
    type: KihonType.kick,
    diff: 1,
    video: true,
  ),
  KihonID.yokogerikek: Kihon(
    id: KihonID.yokogerikek,
    name: "Yoko Geri Kekomi",
    fr: "Yoko Guéri Kékomi",
    jp: "横蹴り蹴込",
    trad: "Coup de pied de côté pénétrant",
    type: KihonType.kick,
    diff: 1,
    nosuffix: true,
    video: true,
  ),
  KihonID.mawashigeri: Kihon(
    id: KihonID.mawashigeri,
    name: "Mawashi",
    jp: "回し",
    trad: "Coup de pied circulaire",
    type: KihonType.kick,
    diff: 1,
    video: true,
  ),

  // NIV 2

  KihonID.fumikomi: Kihon(
    id: KihonID.fumikomi,
    name: "Fumi Komi",
    jp: "踏み込み",
    trad: "Coup de pied écrasant",
    type: KihonType.kick,
    diff: 2,
    noniv: true,
  ),
  KihonID.uchirogeri: Kihon(
    id: KihonID.uchirogeri,
    name: "Ushiro",
    fr: "Oushiro",
    jp: "後ろ",
    trad: "Coup de pied vers l'arrière",
    type: KihonType.kick,
    nomod: true,
    diff: 2,
    video: true,
  ),
  KihonID.yokogerikea: Kihon(
    id: KihonID.yokogerikea,
    name: "Yoko Geri Keage",
    fr: "Yoko Guéri Kéagué",
    jp: "横蹴り蹴上げ",
    trad: "Coup de pied de côté fouetté",
    type: KihonType.kick,
    diff: 2,
    nosuffix: true,
    video: true,
  ),
  KihonID.namigaeshi: Kihon(
    id: KihonID.namigaeshi,
    name: "Nami Gaeshi",
    trad: "Mouvement remontant avec la plante du pied",
    type: KihonType.kick,
    diff: 2,
    nosuffix: true,
    noniv: true,
    excl: [Movement.iki, Movement.dea, Movement.ayu],
  ),

  // NIV 3

  KihonID.hizageri: Kihon(
    id: KihonID.hizageri,
    name: "Hiza",
    jp: "膝",
    trad: "Coup de genou",
    type: KihonType.kick,
    diff: 3,
  ),
  KihonID.fumikiri: Kihon(
    id: KihonID.fumikiri,
    name: "Fumi Kiri",
    // jp: "不身起り"
    trad: "Coup de pied tranchant",
    type: KihonType.kick,
    diff: 3,
    noniv: true,
  ),
  KihonID.uramawashi: Kihon(
    id: KihonID.uramawashi,
    name: "Ura Mawashi",
    jp: "裏回し",
    trad: "Coup de pied circulaire inversé",
    type: KihonType.kick,
    diff: 3,
  ),
  KihonID.mikazukigeri: Kihon(
    id: KihonID.mikazukigeri,
    name: "Mikazuki",
    trad: "Balayage circulaire avec la plante du pied",
    type: KihonType.kick,
    diff: 3,
  ),
  KihonID.tobigeri: Kihon(
    id: KihonID.tobigeri,
    name: "Tobi",
    trad: "Coup de pied sauté",
    type: KihonType.kick,
    diff: 3,
    noniv: true,
    nomod: true,
  ),
  KihonID.ashibarai: Kihon(
    id: KihonID.ashibarai,
    name: "Ashi Barai",
    trad: "Balayage avec le pied",
    type: KihonType.kick,
    diff: 3,
    noniv: true,
    nosuffix: true,
  ),

  // NIV 4

  KihonID.otoshigeri: Kihon(
    id: KihonID.otoshigeri,
    name: "Otoshi",
    jp: "落とし",
    trad: "Coup de haut en bas jambe tendue",
    type: KihonType.kick,
    diff: 4,
    noniv: true,
  ),
  KihonID.nidangeri: Kihon(
    id: KihonID.nidangeri,
    name: "Nidan",
    fr: "Nidanne",
    trad: "Coup de pied sauté double",
    type: KihonType.kick,
    diff: 4,
    noniv: true,
    nomod: true,
  ),
  KihonID.uramikazuki: Kihon(
    id: KihonID.uramikazuki,
    name: "Ura Mikazuki",
    fr: "Oura Mikazuki",
    jp: "裏 Mikazuki",
    trad: "Balayage circulaire inverse avec la plante du pied",
    type: KihonType.kick,
    diff: 4,
  ),
  KihonID.uchirobarai: Kihon(
    id: KihonID.uchirobarai,
    name: "Ushiro Mawashi Barai",
    fr: "Ouchiro Mawashi Baraïlle",
    trad: "Balayage tournant par l'arrière",
    type: KihonType.kick,
    diff: 4,
    nosuffix: true,
    noniv: true,
    nomod: true,
  ),
  KihonID.kakatogeri: Kihon(
    id: KihonID.kakatogeri,
    name: "Kakato",
    trad: "Coup de talon de haut en bas",
    type: KihonType.kick,
    diff: 4,
    noniv: true,
  ),
};