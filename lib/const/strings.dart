/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

class UIString {
  UIString._();

  static const String AppName = "Kihon Trainer";
  static const String AppVer = "0.95";
  static const String AppLic = "Mozilla Public Licence v2.0";
  static const String AppDesc = "Application destinée aux pratiquants de Karaté\n\n"
      "Permet de pratiquer ses Kihon en générant des séquences pseudo aléatoires de techniques et de les dicter à haute voix afin de simuler l'examen de passage de grade.\n\n"
      "Crée par RK en collaboration avec le Karaté Club de Mourenx.";

  static const String InfoErrTitle = "Impossible de générer l'épreuve";
  static const String InfoErrSub = "Aucune partie de l'épreuve n'est active ou aucune technique valide à tirer au sort n'est activée.\n\nVérifiez les options et les techniques puis réessayez.";

  static const String InfoTitle = "Aucune épreuve générée";
  static const String InfoSub = "Personnalisez vos options et vos techniques si besoin puis générez une épreuve pour afficher des séquences de techniques et commencer la dictée vocale.";

  static const String Gen = "Générer une épreuve";
  static const String GenNew = "Générer une nouvelle épreuve";
  static const String Legend = "Légende";
  static const String LegendTitle = "Légende des notations";
  static const String Start = "Commencer";
  static const String Stop = "Arrêter";
  static const String Pause = "Interrompre";
  static const String Resume = "Reprendre";
  static const String Running = "Épreuve en cours...";
  static const String Paused = "Épreuve en pause.";

  static const String NoTTSTitle = "Synthèse vocale inopérante";
  static const String NoTTSContent = "La synthèse vocale ne peut pas fonctionner sur votre appareil. \n\n"
      "Merci de vérifier que l'application \"Synthèse Vocale Google\" ainsi qu'au moins les voix Françaises soient bien installés sur votre appareil puis relancez complètement l'application.";

  static const String Starting = "Démarrage de l'épreuve...";
  static const String Pausing = "Mise en pause de l'épreuve...";
  static const String Stopping = "Arrêt de l'épreuve...";

// Popup intents
  static const String TTSSettings = "Paramètres synthèse vocale";
  static const String GTTSPlayStore = "Synthèse vocale Google (Play Store)";
  static const String LocalVoices = "Voix locales installées";

// First Run Popup
  static const String FirstRunTitle = "Version test";
  static const String FirstRunContent = "Cette application n'est pas encore en version finale. Des bugs peuvent donc subsister.\n\n"
      "Par ailleurs les règles de génération de kihons ne sont pas encore complètes, il est donc probable que certains enchaînements générés soient improbables ou contraires au règlement officiel.\n\n"
      "Dans tous les cas merci de me remonter tout soucis rencontré par e-mail à :\n\n"
      "khirac@protonmail.com";

  static const String Contact = "Contact";
  static const String Gitlab = "Gitlab";
  static const String Ok = "OK";

// tab options
  static const String NumberSelect = "Nombre de répétitions";
  static const String Confirm = "Valider";
  static const String Cancel = "Annuler";

// tab kihons
  static const String KihonTabMain = "Liste des Kihon";
  static const String KihonTabDesc = "Consulter la liste des techniques (Kihon) ou exclure celles non souhaitées pour les prochaines générations d'épreuves.\nCes exclusions n'affectent que le mode Aléatoire ou Hybride.";
  static const String ModsDesc = "Voir les attributs pouvant être imposés à chaque technique.";
  static const String SeqsMain = "Enchaînements";
  static const String SeqsDesc = "Voir les enchaînements de Kihon qui peuvent être inclus dans les modes Prédéfini ou Hybride.";
  static const String PreSeqs = "Enchaînements";
  static const String ReinitDesc = "Restaurer les paramètres par défaut.\nCeci réactivera toutes les techniques exclues.";
  static const String ReinitButton = "Réinitialiser";
  static const String ResetDialogTitle = "Réinitialisation";
  static const String ResetDialogText = "Cette action effacera vos paramètres et réactivera toutes les techniques exclues.\nSouhaitez-vous continuer ?";

  // Widget Vidéo
  static const String NoVideo = "Démonstration vidéo non disponible";
  static const String VideoLoading = "Chargement de la vidéo en cours...";
  static const String VideoDL = "Téléchargement de la vidéo en cours...";
  static const String VideoDLErr = "La vidéo n'a pas pu être téléchargée";

  static const String FirstTTSTitle = "";
  static const String FirstTTSContent = "";

  static const String Loading = "Initialisation en cours...";

  // Assets & links
  static const String AppIcon = "assets/img/appicon.png";
  static const String KihonTabIcon = "assets/ttf/tabicon_kihons.ttf";
  static const String BeepSnd = "assets/snd/beep-s.wav";
  static const String MainBG = "assets/img/bg_main.png";
  static const String DefBG = "assets/img/bg_def.png";
  static const String HandBG = "assets/img/bg_hand.png";
  static const String KickBG = "assets/img/bg_kick.png";
  static const String DefIcon = "assets/img/icon_def.png";
  static const String HandIcon = "assets/img/icon_hand.png";
  static const String KickIcon = "assets/img/icon_kick.png";
  static const String SeqsIcon = "assets/img/icon_seqs.png";
  static const String FlagFR = "assets/img/flag_fr.png";
  static const String FlagJP = "assets/img/flag_jp.png";

  static const String AppSrc = "https://gitlab.com/Scias/kihon-trainer";
  static const String AppMail = "khirac@protonmail.com";

  // Android intents TTS
  static const String SettingsPackage = "com.android.settings";
  static const String TTSPackage = "com.google.android.tts";
  static const String IntentAction = "android.intent.action.MAIN";
  // com.android.settings.TTS_SETTINGS
  static const String TTSIntentSettings = "com.android.settings.Settings\$TextToSpeechSettingsActivity";
  // com.android.settings.TextToSpeechSettings
  static const String TTSIntentLocalVoices = "com.google.android.tts.local.voicepack.ui.VoiceDataInstallActivity";
  static const String GTTSPlayStoreAction = "action_view";
  static const String GTTSPlayStoreLink = "https://play.google.com/store/apps/details?id=com.google.android.tts";

  // Source video
  static const String VideoSrcBase = "https://firebasestorage.googleapis.com/v0/b/kihon-trainer.appspot.com/o/video%2F";

}